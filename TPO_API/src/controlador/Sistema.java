package controlador;


import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import persistencia.AdmPersistenciaCliente;
import model.*;
import persistencia.*;

public class Sistema {

	// Atributos
	private Mapa mapa;
	private Vector<Contrato> contratos;
	private Vector<Banco> bancos;
	private Vector<Cliente> clientes;
	private Vector<Abono> abonos;
	private static Sistema instancia;

	// Constructor
	public Sistema() {

		this.mapa = new Mapa();		
		this.clientes = new Vector<Cliente>();

		//Se cargan todos los Abonos, Bancos y Contratos ACTIVOS para calcular la disponibilidad y mostrar comboBox
		this.abonos = AdmPersistenciaAbono.getInstancia().selectAll();
		this.bancos = AdmPersistenciaBanco.getInstancia().selectAll();
		this.contratos = AdmPersistenciaContrato.getInstancia().selectAll();			
	}

	public static Sistema getInstancia() {
		if (instancia == null)
			instancia = new Sistema();
		return instancia;
	}

	/*******************************************************************************************************/
	/* BUSQUEDAS SIMPLES */
	/******************************************************************************************************/

	//Buscar un usuario en BD
	private Usuario buscarUsuario(String alias){
		return AdmPersistenciaUsuario.getInstancia().select(alias);
	}
	
	public UsuarioView buscarUsuarioView(String alias){
		Usuario u = buscarUsuario(alias);
		
		if (u != null)
			return u.getView();
		else
			return null;
	}
	
	public List<UsuarioView> buscarUsuariosView(){
		
		List<Usuario> usuarios = AdmPersistenciaUsuario.getInstancia().selectAll();
		List<UsuarioView> usuariosView = new ArrayList<UsuarioView>();
		
		for (Usuario u : usuarios)
			if ( !u.getPermiso().equals(Permiso.Master) )
				usuariosView.add( u.getView() );
		
		return usuariosView;
	}
	
	// Devuelve un cliente determinado
	private Cliente buscarCliente(int DNI) {
		// Busco en memoria
		for (Cliente cliente : clientes)
			if (cliente.getDNI() == DNI)
				return cliente;

		// Busco el cliente en la BD
		Cliente cliente = AdmPersistenciaCliente.getInstancia().select(DNI);
		if (cliente != null)
			return cliente;
		else
			return null;

	}

	// Devuelve una view de cliente
	public ClienteView buscarClienteView(int DNI) {
		Cliente cliente = buscarCliente(DNI);

		if (cliente != null)
			return cliente.getClienteView();
		else
			return null;
	}
	
	public List<ClienteView> buscarClientesView(){
		
		Vector<Cliente> todos = AdmPersistenciaCliente.getInstancia().selectAll();
		List<ClienteView> clientesView = new ArrayList<ClienteView>();
		
		for (Cliente c: todos)
			clientesView.add( c.getClienteView() );
		
		return clientesView;
	}

	// Devuelve un abono determinado
	private Abono buscarAbono(String nombre) {
		// Busco en memoria
		for (Abono abono : abonos)
			if (abono.getNombre().equals(nombre))
				return abono;

		// Busco el abono en la BD
		Abono abono = AdmPersistenciaAbono.getInstancia().select(nombre);
		if (abono != null)
			return abono;
		else
			return null;

	}

	// Devuelve una view de abono
	public AbonoView buscarAbonoView(String nombre) {
		Abono abono = buscarAbono(nombre);

		if (abono != null)
			return abono.getAbonoView();
		else
			return null;
	}
	
	public List<AbonoView> buscarAbonosView(){
		List<AbonoView> abonosView = new ArrayList<AbonoView>();
		
		for (Abono a : abonos)
			abonosView.add(a.getAbonoView() );
		
		return abonosView;
	}

	// Devuelve un banco determinado
	private Banco buscarBanco(String banco) {
		// Busco en memoria
		for (Banco b : bancos)
			if ( b.getBanco().equals(banco) )
				return b;

		// Busco el cliente en la BD
		Banco b = AdmPersistenciaBanco.getInstancia().select(banco);
		if (b != null)
			return b;
		else
			return null;

	}

	// Devuelve una view de banco
	public BancoView buscarBancoView(String nombre) {
		Banco banco = buscarBanco(nombre);

		if (banco != null)
			return banco.getMedioPagoView();
		else
			return null;
	}
	
	public List<BancoView> buscarBancosView(){
		
		List<BancoView> bancosView = new ArrayList<BancoView>();
		
		for (Banco b : bancos)
			bancosView.add( b.getMedioPagoView() );
		
		return bancosView;

	}
	
	//Devuelve una cochera determinada, si no existe devuelve null
	private Cochera buscarCochera (int nroCochera){
		Vector<Cochera> cocheras = mapa.getCocheras();
		for (Cochera cochera : cocheras)
			if (cochera.getNroCochera() == nroCochera)
				return cochera;
		
			return null;
	}

	// Devuelve un contrato determinado
	private Contrato buscarContrato(int nroContrato) {
		// Busco en memoria
		for (Contrato contrato : contratos)
			if (contrato.getNroContrato() ==nroContrato )
				return contrato;

		// Busco el cliente en la BD
		Contrato contrato = AdmPersistenciaContrato.getInstancia().select(nroContrato);
		if (contrato != null)
			return contrato;
		else
			return null;

	}

	// Devuelve una view de contrato
	public ContratoView buscarContratoView(int nroContrato) {
		Contrato contrato = buscarContrato(nroContrato);

		if (contrato != null)
			return contrato.getContratoView();
		else
			return null;
	}
	
	// Devuelve una view de contrato
	public List<ContratoView> buscarContratosView() {
		
		List<Contrato> todosContratos = AdmPersistenciaContrato.getInstancia().selectAll();
		List<ContratoView> todosContratosView = new ArrayList<ContratoView>();

		if (todosContratos != null)
			for (Contrato c : todosContratos)
				todosContratosView.add( c.getContratoView() );
			
		return todosContratosView;			
	}
	
	public ContratoView buscarContratoPatenteView(String patente) {
		
		for (Contrato c : contratos)
			if (c.getAuto().getPatente().equals(patente))
				return c.getContratoView();
		return null;
	} 

	// Indica si la patente tiene algun contrato activo
	public boolean existePatente(String patente) {
		for (Contrato contrato : contratos)
			if (contrato.getAuto().getPatente().equals(patente))
				return true;

		return false;
	}

	// Indica si existe un cliente determinado
	public boolean existeCliente(int DNI) {
		// Busco en memoria
		for (Cliente cliente : clientes)
			if (cliente.getDNI() == DNI)
				return true;

		// Busco el cliente en la BD
		Cliente cliente = AdmPersistenciaCliente.getInstancia().select(DNI);
		if (cliente != null)
			return true;
		else
			return false;
	}
	
	// Devuelve todos los nombres de los bancos activos 
	public Vector<String> getBancos(){
		Vector<String> vector = new Vector<String>(); 
		
		for (Banco banco : bancos)
			vector.add(banco.getBanco());
		
		return vector;
			
	}
	
	public List<String> getMarcas(){
		return AdmPersistenciaAuto.getInstancia().selectAll();
	}

	/*******************************************************************************************************/
	/* BUSQUEDAS AVANZADAS */
	/******************************************************************************************************/

	// Devuelve una cochera libre segun el tipo de auto (Auto o Camioneta). Si
	// no hay devuelve null
	private Cochera buscarCocheraLibre(String tipo,Abono abono) {

		Cochera cochera;

		if (tipo.equals("Auto"))
			cochera = mapa.getCocheraSimplelLibre(abono);
		else if (tipo.equals("Camioneta"))
			cochera = mapa.getCocheraDobleLibre(abono);
		else
			cochera = null;

		return cochera;
	}

	/*
	 * Verifica si hay disponibilidad para vender un abono o contrato mas, y
	 * devuelve un vector de String con los nombres de los abonos encontrados
	 * (asi lo puedo agregar a un comboBox en la ventana)
	 */
	public Vector<String> buscarDisponibilidad() {
		Vector<String> nombres = new Vector<String>();

		int cantD = 0;
		int cantQ = 0;
		int cantM = 0;

		//Calculo los % de venta
		long maxD = Double.doubleToLongBits(mapa.getCocheras().size() * 0.25); //25 
		long maxQ = Double.doubleToLongBits(mapa.getCocheras().size() * 0.25); //25
		long maxM = Double.doubleToLongBits(mapa.getCocheras().size() * 0.50); //50

		//Cuento los abonos vendidos
		for (Contrato contrato : contratos) {

			if (contrato.getAbono().getHoras() == 24)
				cantD++;

			if (contrato.getAbono().getHoras() > 24 && contrato.getAbono().getHoras() <= 360)
				cantQ++;

			if (contrato.getAbono().getHoras() > 360)
				cantM++;

		}
		
		//Valido cuales abonos puedo vender 
		for (Abono abono : abonos) {

			if (abono.getHoras() < 24 && mapa.getCantCocherasLibres() >= 1)
				nombres.addElement(abono.getNombre());

			if (abono.getHoras() == 24 && cantD < maxD)
				nombres.addElement(abono.getNombre());

			if (abono.getHoras() > 24 && abono.getHoras() <= 360
					&& cantQ < maxQ)
				nombres.addElement(abono.getNombre());

			if (abono.getHoras() > 360 && cantM < maxM)
				nombres.addElement(abono.getNombre());

		}

		return nombres;
	}

	// Busca la patente en los contratos, luego busca una cochera libre, la bloquea y la asocia al contrato
	public boolean bloquearCochera(String patente) {

		for (Contrato contrato : contratos) {
			if (contrato.getAuto().getPatente().equals(patente)) {
				if (contrato.getCochera() == null) {
					Cochera cochera = buscarCocheraLibre( contrato.getAuto().getTipo() , contrato.getAbono() );
					contrato.setCochera(cochera);

					// Actualizo en BD
					contrato.update();
					return true;
				}
			}
		}

		return false;

	}

	// Busca la patente en los contratos, luego libera la cochera asociada y la desasocia del contrato
	public boolean desbloquearCochera(String patente) {

		for (Contrato contrato : contratos) {
			
			if (contrato.getAuto().getPatente().equals(patente)) {
				
				if (contrato.getCochera() != null) {

					// Libero la cochera
					int nroCochera = contrato.getCochera().getNroCochera();
					contrato.setCochera(null);
					mapa.liberarCochera(nroCochera);

					// Actualizo en BD
					contrato.update();
					
					return true;
				}
			}
		}	
		
		return false;
	}
	

	/******************************************************************************************************/
	/* CREACIONES */
	/******************************************************************************************************/
	
	//Crea el usuario
	public boolean crearUsuario(UsuarioView uv){
		
		Usuario u = buscarUsuario(uv.getAlias());
		
		if (u == null){			
			
			u = new Usuario( uv.getAlias() , uv.getPassword() , uv.getNombreReal() , uv.getPermiso() , true);
			u.insert();
			return true;			
		}
		else
			return false;
		
	}

	// Crea el cliente
	public boolean crearCliente(int DNI, String nombre, String domicilio,int telefono, String mail) {
		
		Cliente cliente = AdmPersistenciaCliente.getInstancia().selectAB(DNI);
		
		if (cliente == null){
			cliente = new Cliente(DNI, nombre, domicilio, telefono, mail);
			clientes.addElement(cliente);
			cliente.insert();
			return true;			
		}
		else
			if ( !cliente.estaActivo() ){
				cliente.setNombre(nombre);
				cliente.setDomicilio(domicilio);
				cliente.setMail(mail);
				cliente.setActivo(true);
				clientes.addElement(cliente);
				cliente.update();
				return true;
			}
			else
				return false;
							
	}
	
	// Creo un abono con los datos (debo validar si me ingresaron dias y horas)
	public boolean crearAbono(String nombre, int horas, float precio,float descuento) {
		Abono abono = AdmPersistenciaAbono.getInstancia().selectAB(nombre);
		
		if (abono == null){
			
			abono = new Abono(nombre, horas, precio, descuento);
			abonos.addElement(abono);
			abono.insert();
			return true;
		}
		else
			if ( !abono.estaActivo() ){
				abono.setHoras(horas);
				abono.setPrecio(precio);
				abono.setDescuento(descuento);
				abono.setActivo(true);
				abonos.addElement(abono);
				abono.update();
				return true;				
			}
			else
				return false;
		
	}

	// Creo un banco con los datos
	public boolean crearBanco(String banco, String url,String archivo) {
		
		Banco b = AdmPersistenciaBanco.getInstancia().selectAB(banco);
		
		if(b == null){
			b = new Banco(banco, url, archivo);
			bancos.addElement(b);
			b.insert();
			return true;			
		}
		else
			if ( !b.estaActivo() ){
				b.setUrlConexion(url);
				b.setNombreArchivo(archivo);				
				b.setActivo(true);
				bancos.addElement(b);
				b.update();				
				return true;
				
			}
			else
				return false;
	}
	
	public Auto crearAuto(String patente,String marca, String modelo, String tipo){
		
		Auto auto = AdmPersistenciaAuto.getInstancia().selectAB(patente);
		
		if (auto == null){
			auto = new Auto(patente, marca, modelo, tipo);
			auto.insert();
			return auto;			
		}
		else
			if ( !auto.estaActivo() ){
				auto.setMarca(marca);
				auto.setModelo(modelo);
				auto.setTipo(tipo);
				auto.setActivo(true);				
				auto.update();
				return auto;
			}
			else
				return null;
		
	}

	/* Primero crea el auto, busca el cliente, busca el banco, busca el
	 * abono y por ultimo crea el contrato con estos datos.	 */
	public boolean crearContrato(int DNI, String fechaDesde, String patente,
			String marca, String modelo, String tipo, String pago,
			String banco, String nroCheque, int nroTarjeta, String fechaVto,
			int CBU, String nombreAbono) {
		
		if ( !existePatente(patente) ){
			
			Abono abono = buscarAbono(nombreAbono);
			Cochera cochera = buscarCocheraLibre(tipo , abono);
			
			if (cochera  != null){
			
				Cliente cliente = buscarCliente(DNI);
								
				
				Timestamp fechaDesdeFormateada = convertirStringToDate(fechaDesde);
				Timestamp fechaVtoFormateada = null;
				Timestamp fechaHasta;
			
				// Convierto fechaVto
				if (!fechaVto.isEmpty())
					fechaVtoFormateada = convertirStringToDate(fechaVto);
		
				/*Completo fechaHasta segun el abono. Si horas < 24 lo considero como abono HORA y no completo la fecha */
				if (abono.getHoras() < 24)
					fechaHasta = null;
				else
					fechaHasta = calcularFecha(abono, fechaDesdeFormateada);
		
				// Creo el auto en la BD
				Auto auto = crearAuto(patente,marca,modelo,tipo);
				
				Contrato contrato = null;
		
				switch (pago) {
				case "Efectivo":
					ContratoEfectivo contratoE = new ContratoEfectivo(0,cliente,fechaDesdeFormateada, fechaHasta, auto, cochera, abono,false,true);
					contratoE.insert();										
					break;
		
				case "Cheque":
					ContratoCheque contratoC = new ContratoCheque(0,cliente,fechaDesdeFormateada,fechaHasta, auto,cochera,abono,banco,nroCheque,fechaVtoFormateada,false,true);					
					contratoC.insert();
					break;
		
				case "Tarjeta":
					ContratoTarjeta contratoT = new ContratoTarjeta(0,cliente,fechaDesdeFormateada, fechaHasta, auto, cochera, abono,	banco, nroTarjeta, fechaVtoFormateada,false,true);
					contratoT.insert();					
					break;
				}
				
				contrato = AdmPersistenciaContrato.getInstancia().select(patente);
				
				if (abono.getHoras() > 24){
					float total = calcularTotalCobrar( contrato.getAbono() , contrato.getFechaDesde(), contrato.getFechaHasta() );
					float cantHoras = calcularHoras(contrato.getFechaDesde(),contrato.getFechaHasta() );
					crearRecibo(fechaDesdeFormateada, contrato.getCliente().getDNI() ,contrato.getAbono().getNombre() , cantHoras , total);
					contrato.setPago(true);
					contrato.update();
				}
				
				contratos.addElement(contrato);
					
				return true;
				
			}

			
		}
		
		return false;

		
	}
	
	
	//Crea el recibo en la BD 
	public void crearRecibo(Timestamp fecha, int dni, String nombreAbono, float h, float t){
		AdmPersistenciaRecibo.getInstancia().insert(fecha, dni , nombreAbono, h, t);
	}
		
	
	//Crea el reporte
	private Reporte crearReporte(Timestamp fechaDesde,Timestamp fechaHasta){
		
		Reporte r = null;
		List<Recibo> recibos = AdmPersistenciaRecibo.getInstancia().selectAll(fechaDesde, fechaHasta);
		
		if (recibos != null){								
			r = new Reporte(fechaDesde,fechaHasta,recibos);
		}
		
		return r;
		
	}
		
	//Obtiene el reporte view
	public ReporteView crearReporteView(Timestamp fechaDesde,Timestamp fechaHasta){		
		Reporte r = crearReporte(fechaDesde,fechaHasta); 		
		return r.toView();
	}
		

	/******************************************************************************************************/
	/* MODIFICACIONES */
	/******************************************************************************************************/

	// Modifica un cliente determinado (el DNI no se puede modificar)
	public void modificarCliente(int DNI, String nombre, String domicilio,int telefono, String mail) {
		
		Cliente cliente = buscarCliente(DNI);

		if (cliente != null) {
			cliente.setNombre(nombre);
			cliente.setDomicilio(domicilio);
			cliente.setTel(telefono);
			cliente.setMail(mail);
			
			for (Contrato c : contratos)
				if (c.getCliente().getDNI() == cliente.getDNI())
					c.setCliente(cliente);

			// Modifico en la BD
			cliente.update();
		}
	}

	// Modifica un abono determinado (debo validar si me ingresaron dias u horas)
	public void modificarAbono(String nombre, int horas, float precio,float descuento) {
		
		Abono abono = buscarAbono(nombre);

		if (abono != null) {
			abono.setHoras(horas);
			abono.setPrecio(precio);
			abono.setDescuento(descuento);
			
			for (Contrato c : contratos)
				if (c.getAbono().getNombre().equals(nombre))
					c.setAbono(abono);
			

			// Modifico en la BD
			abono.update();
		}

	}
	public void modificarCochera (int nroCochera, String estado){
		Cochera cochera = buscarCochera (nroCochera);
		if (cochera != null){ 
			// La cochera Existe
			if ( estado.equals("Verdadero") )
				cochera.setLibre(true);
			else
				cochera.setLibre(false);
			
			cochera.updateEstado();			
		}
	
	}


	/******************************************************************************************************/
	/* ELIMINACIONES */
	/******************************************************************************************************/

	//Elimina un usuario
	public boolean eliminarUsuario(String alias){
		
		Usuario usuario = buscarUsuario(alias);
		
		if (usuario != null && !usuario.getPermiso().equals(Permiso.Master) ){
			
			usuario.delete();			
			return true;
		}
		else
			return false;
		
	}
	
	// Elimina un cliente determinado 
	public boolean eliminarCliente(int DNI) {
		Cliente cliente = buscarCliente(DNI);
		
		if (cliente != null) {
			
			for (Contrato c : contratos)
				if (c.getCliente().getDNI() == DNI)
					return false;
			
			clientes.remove(cliente);
			cliente.delete();
			return true;
		}
		else
			return false;		
	}

	// Elimina un abono determinado
	public boolean eliminarAbono(String nombre) {
		Abono abono = buscarAbono(nombre);

		if (abono != null) {
			
			for (Contrato c : contratos)
				if (c.getAbono().getNombre().equals(nombre) )
					return false;
			
			abonos.remove(abono);
			abono.delete();
			return true;
		}
		else
			return false;
	}

	// Elimina un banco determinado
	public boolean eliminarBanco(String banco) {
		Banco b = buscarBanco(banco);

		if (b != null) {
			
			for (Contrato c : contratos){
				
				if ( c.sosTarjeta() ){
					if ( ((ContratoTarjeta)c).getBanco().equals(banco) )
						return false;				
				}
				
		
			}
			
			bancos.remove(b);
			b.delete();
			return true;
		}
		else
			return false;
	}

	// Elimina un contrato determinado
	public boolean eliminarContrato(int nroContrato) {
		Contrato contrato = buscarContrato(nroContrato);

		if (contrato != null) {
							
			if (contrato.getCochera() != null)
				mapa.liberarCochera(contrato.getCochera().getNroCochera());

			contratos.remove(contrato);
			contrato.delete();
			contrato.getAuto().delete();
			
			return true;

		}

		return false;
		
	}
	
	
	// Elimina un contrato determinado
		public void eliminarCochera (int nroCochera) {
			Cochera cochera = buscarCochera (nroCochera);

			if (cochera != null) {
				
				mapa.liberarCochera(nroCochera);
				
			}
		}

	/******************************************************************************************************/
	/* COBRANZA */
	/******************************************************************************************************/
		
	// Cobra un contrato determinado y devuelve el total
	public float cobrarContrato(int nroContrato) {		
		
		Contrato contrato = buscarContrato(nroContrato);
		
		if (contrato != null && !contrato.estaPago() ){
			
		java.util.Date date= new java.util.Date();
		Timestamp fecha = new Timestamp(date.getTime());
			
		if (contrato.getFechaHasta() == null){	
			contrato.setFechaHasta(fecha);
		}
		
		float total = calcularTotalCobrar( contrato.getAbono() , contrato.getFechaDesde(), fecha );
		float cantHoras = calcularHoras(contrato.getFechaDesde(),fecha );
		
		crearRecibo(fecha, contrato.getCliente().getDNI() ,contrato.getAbono().getNombre() , cantHoras , total);
		
		contrato.setPago(true);
		contrato.update();
						
		//Doy de baja el contrato en memo y BD		
		contratos.remove(contrato);
		eliminarContrato(nroContrato);

		return total;
					
		}
		
		return 0;			
	}
	
	// Calcula el total a cobrar de un abono entre 2 fechas 
	private float calcularTotalCobrar(Abono abono,Timestamp fechaDesde,Timestamp fechaHasta){	
		
		float cantHoras = calcularHoras(fechaDesde,fechaHasta);
		
		return abono.calcularTotal(cantHoras);
		
	}


	/******************************************************************************************************/
	/* VALIDACIONES y CONVERSIONES */
	/******************************************************************************************************/

	// Convierte un String a Fecha
	public Timestamp convertirStringToDate(String fecha) {
		return java.sql.Timestamp.valueOf(fecha);
	}

	// Convierte una fecha a String
	public String convertirDateToString(Date fecha) {
		return fecha.toString();
	}

	// Verifica si el string contiene solo numeros
	public boolean esNumerico(String str) {
		return (str.matches("[+-]?\\d*(\\.\\d+)?") && !str.equals(""));
	}

	// Verifica si la fecha tiene el siguiente formato
	public boolean validarFecha(String fecha) {
		// aaaa-mm-dd hh:mm:ss
		return fecha.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]");
	}

	// Suma las horas del abono a la fecha pasados por parametro
	private Timestamp calcularFecha(Abono abono, Timestamp fecha) {

		Calendar calendario = Calendar.getInstance();
		calendario.setTimeInMillis(fecha.getTime());

		calendario.add(Calendar.HOUR, abono.getHoras());

		return new Timestamp(calendario.getTimeInMillis());

	}
	
	public boolean validarMail(String mail){
		
		String patron = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(patron);		
		Matcher matcher = pattern.matcher(mail);
		
		return matcher.matches();
		
	}
	
	public boolean validarFechaVto(String fecha){
		
		int mesActual = Calendar.getInstance().get(Calendar.MONTH)+1;
		int anioActual = Calendar.getInstance().get(Calendar.YEAR);
		
		int mesT = Integer.valueOf(fecha.substring(5,7) );
		int anioT = Integer.valueOf(fecha.substring(0,4) );

		boolean resultado = false;
		
		if ( anioT > anioActual )
			resultado = true;
		else
			if ( anioT == anioActual){
				
				if (mesT >= mesActual)
					resultado = true;
				else
					resultado = false;
				
			}
			else
				resultado = false;

		return resultado;
		
	}
			
	// Devuelve la cantidad de horas entre 2 fechas
	public float calcularHoras(Timestamp fechaDesde, Timestamp fechaHasta) {
		
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		
		cal1.setTimeInMillis(fechaDesde.getTime());
		cal2.setTimeInMillis(fechaHasta.getTime());
		
		float diferencia = cal2.getTimeInMillis() - cal1.getTimeInMillis();
		
		return diferencia / (60 * 60 * 1000);				
	}
	

	// Verifica que la fecha este dentro de los fechasDesde y fechaHasta del
	// contrato. Si es abono hora cobra.
	public boolean esContratoVencido(String patente, Timestamp fecha) {
		
		Contrato contrato = null;
		for (Contrato c : contratos)
			if (c.getAuto().getPatente().equals(patente)){
				contrato = c;
				break;
			}

		// Verifico si es abono Hora y devuelvo vencido
		if (contrato.getAbono().getHoras() < 24 || contrato.getAbono().getHoras() == 24)
			return true;

		// Verifico si fecha >= fechaDesde y fecha <= fechaHasta
		if (contrato.getFechaDesde().before(fecha) && contrato.getFechaHasta().after(fecha))
			return false;
		else
			return true;
	}
	
	public Permiso validarUsuario(String alias,char[] pass){
		
		Usuario u = buscarUsuario(alias);
		
		String p = String.valueOf(pass);
		
		if ( u != null && u.getPassword().equals(p) ){
			p = ""; //limpio por seguridad
			return u.getPermiso();
			
		}
		else
			return Permiso.Ninguno;
	}

}
