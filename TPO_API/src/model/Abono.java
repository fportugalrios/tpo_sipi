package model;

import persistencia.AdmPersistenciaAbono;

public class Abono {
	
	//Atributos
	private String nombre;
	private int horas;
	private float precio;
	private float descuento;
	private boolean activo;	
	
	//Constructores
	public Abono(String nombre, int horas,float precio, float descuento) {
		super();
		this.nombre = nombre;
		this.horas = horas;
		this.precio = precio;
		this.descuento = descuento;
		this.activo = true;
	}
	
	public Abono(String nombre, int horas,float precio, float descuento,boolean activo) {
		super();
		this.nombre = nombre;
		this.horas = horas;
		this.precio = precio;
		this.descuento = descuento;
		this.activo = activo;
	}

	//Getters & Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getHoras() {
		return this.horas;
	}
	
	public void setHoras(int horas) {
		this.horas = horas;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	public float getDescuento() {
		return descuento;
	}
	
	public void setDescuento(float descuento) {
		this.descuento = descuento;
	}

	public boolean estaActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	public float calcularTotal(float cant){
							
		if ( this.horas < 24 ){
					
			if ( cant < (float)this.horas/2 )
				return this.precio - this.descuento;
			else
				return cant * this.precio;							
		}
		else{
			
			if ( cant < (float)this.horas/2 )
				return this.precio - this.descuento;
			else
				return this.precio;
			
		}			
			 			 
	}
	
	public AbonoView getAbonoView(){
		return new AbonoView(nombre,horas,precio,descuento,activo);
	}
	
	//Metodos de persistencia	
	public void insert(){
		AdmPersistenciaAbono.getInstancia().insert(this);
	}
	
	public void update(){
		AdmPersistenciaAbono.getInstancia().update(this);
	}
	
	public void delete(){
		AdmPersistenciaAbono.getInstancia().delete(this);
	}

}
