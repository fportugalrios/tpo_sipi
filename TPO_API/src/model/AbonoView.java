package model;

public class AbonoView {
	
	//Atributos
	private String nombre;
	private int horas;
	private float precio;
	private float descuento;
	private boolean activo;
	
	//Constructor
	public AbonoView(String nombre, int horas, float precio, float descuento, boolean activo) {
		super();
		this.nombre = nombre;
		this.horas = horas;
		this.precio = precio;
		this.descuento = descuento;
		this.activo = activo;
	}

	//Getters & Setters
	public String getNombre() {
		return nombre;
	}
	
	public int getHoras() {
		return horas;
	}

	public float getPrecio() {
		return precio;
	}
	
	public float getDescuento() {
		return descuento;
	}
	
	public boolean estaActivo() {
		return activo;
	}


}
