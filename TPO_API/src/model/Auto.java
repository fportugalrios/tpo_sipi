package model;

import persistencia.AdmPersistenciaAuto;

public class Auto {
	
	//Atributos
	private String patente;
	private String marca;
	private String modelo;
	private String tipo;
	private boolean activo;
	
	//Constructores
	public Auto(String patente, String marca, String modelo, String tipo) {
		this.patente = patente;
		this.marca = marca;
		this.modelo = modelo;
		this.tipo = tipo;
		this.activo = true;
	}
	
	public Auto(String patente, String marca, String modelo, String tipo,boolean activo) {
		this.patente = patente;
		this.marca = marca;
		this.modelo = modelo;
		this.tipo = tipo;
		this.activo = activo;
	}

	//Getters & Setters
	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public AutoView getAutoView(){
		return new AutoView(patente,marca,modelo,tipo);
	}
	
	public boolean estaActivo(){
		return this.activo;
	}
	
	public void setActivo(boolean activo){
		this.activo = activo;
	}
	
	
	//Metodos de persistencia
	public void insert(){
		AdmPersistenciaAuto.getInstancia().insert(this);
	}
	
	public void update(){
		AdmPersistenciaAuto.getInstancia().update(this);
	}
	
	public void delete(){
		AdmPersistenciaAuto.getInstancia().delete(this);
	}
	
	

}
