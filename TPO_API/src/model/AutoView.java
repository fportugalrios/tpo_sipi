package model;

public class AutoView {
	
	//Atributos
	private String patente;
	private String marca;
	private String modelo;
	private String tipo;
	
	//Constructor
	public AutoView(String patente, String marca, String modelo, String tipo) {
		this.patente = patente;
		this.marca = marca;
		this.modelo = modelo;
		this.tipo = tipo;
	}

	//Getters & Setters
	public String getPatente() {
		return patente;
	}

	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public String getTipo() {
		return tipo;
	}

	
	
	

}
