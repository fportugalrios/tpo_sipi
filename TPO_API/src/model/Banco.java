package model;

import persistencia.AdmPersistenciaBanco;

public class Banco {
	
	//Atributos
	private String banco;
	private String urlConexion;
	private String nombreArchivo;
	private boolean activo;
	
	public Banco(String banco, String urlConexion,String nombreArchivo) {
		super();
		this.banco = banco;
		this.urlConexion = urlConexion;
		this.nombreArchivo = nombreArchivo;
		this.activo = true;	
	}
	
	public Banco(String banco, String urlConexion,String nombreArchivo,boolean activo) {
		super();
		this.banco = banco;
		this.urlConexion = urlConexion;
		this.nombreArchivo = nombreArchivo;
		this.activo = activo;
	}

	//Getters & Setters
	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getUrlConexion() {
		return urlConexion;
	}

	public void setUrlConexion(String urlConexion) {
		this.urlConexion = urlConexion;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public boolean estaActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	public BancoView getMedioPagoView(){
		return new BancoView(banco,urlConexion,nombreArchivo,activo);
	}
	
	//Metodos de persistencia
	public void insert(){
		AdmPersistenciaBanco.getInstancia().insert(this);
	}
	
	public void update(){
		AdmPersistenciaBanco.getInstancia().update(this);
	}
	
	public void delete(){
		AdmPersistenciaBanco.getInstancia().delete(this);
	}

}
