package model;

public class BancoView {
	
	//Atributos
	private String banco;
	private String urlConexion;
	private String nombreArchivo;
	private boolean activo;
	
	//Constructor
	public BancoView(String banco, String urlConexion,String nombreArchivo, boolean activo) {
		super();
		this.banco = banco;
		this.urlConexion = urlConexion;
		this.nombreArchivo = nombreArchivo;
		this.activo = activo;
	}

	//Getters & Setters

	public String getBanco() {
		return banco;
	}

	public String getUrlConexion() {
		return urlConexion;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public boolean estaActivo() {
		return activo;
	}	

}
