package model;

import persistencia.AdmPersistenciaCliente;

public class Cliente {
	
	//Atributos
	private int  DNI;
	private String nombre;
	private String domicilio;
	private int telefono;
	private String mail;
	private boolean activo; 
	
	//Constructores
	public Cliente(int DNI,String nombre,String domicilio,int tel,String mail){
		this.DNI = DNI;
		this.nombre = nombre;
		this.domicilio = domicilio;
		this.telefono = tel;
		this.mail = mail;
		this.activo = true;	
	}
	
	public Cliente(int DNI,String nombre,String domicilio,int tel,String mail,boolean activo){
		this.DNI = DNI;
		this.nombre = nombre;
		this.domicilio = domicilio;
		this.telefono = tel;
		this.mail = mail;
		this.activo = activo;	
	}

	//Getters & Setters	
	public int getDNI() {
		return DNI;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public int getTel() {
		return telefono;
	}

	public void setTel(int tel) {
		this.telefono = tel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean estaActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	public ClienteView getClienteView(){
		return new ClienteView(DNI,nombre,domicilio,telefono,mail);
	}
	
	//Metodos de persistencia
	public void insert(){
		AdmPersistenciaCliente.getInstancia().insert(this);
	}
	
	public void update(){
		AdmPersistenciaCliente.getInstancia().update(this);
	}
	
	public void delete(){
		AdmPersistenciaCliente.getInstancia().delete(this);
	}
	
	

}
