package model;

public class ClienteView {
	
	//Atributos
		private int  DNI;
		private String nombre;
		private String domicilio;
		private int telefono;
		private String mail;
		private boolean activo; 
		
		//Constructor
		public ClienteView(int DNI,String nombre,String domicilio,int tel,String mail){
			this.DNI = DNI;
			this.nombre = nombre;
			this.domicilio = domicilio;
			this.telefono = tel;
			this.mail = mail;
		}

		//Getters & Setters
		public int getDNI() {
			return DNI;
		}
		
		public String getNombre() {
			return nombre;
		}

		public String getDomicilio() {
			return domicilio;
		}

		public int getTel() {
			return telefono;
		}

		public String getMail() {
			return mail;
		}

		public boolean estaActivo() {
			return activo;
		}


}
