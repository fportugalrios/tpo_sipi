package model;

import persistencia.AdmPersistenciaCochera;

public abstract class Cochera {
	
	//Atributos
	protected int nroCochera;
	protected int fila;
	protected int piso;
	protected boolean libre;

	
	//Getters & Setters	
	public int getNroCochera() {
		return nroCochera;
	}

	public void setNroCochera(int nroCochera) {
		this.nroCochera = nroCochera;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public int getPiso() {
		return piso;
	}

	public void setPiso(int piso) {
		this.piso = piso;
	}

	public boolean estaLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}
	
	public abstract CocheraView getView();

	//Metodos de persistencia
	public void insert(){
		AdmPersistenciaCochera.getInstancia().insert(this);
	}
	
	public void updateEstado(){
		AdmPersistenciaCochera.getInstancia().update(this);
	}
	
	
	public void updateDoble(int c){
		AdmPersistenciaCochera.getInstancia().updateDoble(this,c);
	}


}
