package model;

import java.util.Vector;

public class CocheraDoble extends Cochera {
	
	//Atributos
	private Vector<Cochera> cocheras;

	public CocheraDoble(CocheraSimple c1,CocheraSimple c2,boolean estado) {
		super();
		
		this.nroCochera = c1.getNroCochera();
		this.fila = c1.getFila();
		this.piso = c1.getPiso();
				
		this.cocheras = new Vector<Cochera>();
		this.cocheras.addElement(c1);
		this.cocheras.addElement(c2);
		
		this.libre = estado;
	}

	//Getters & Setters
	public Vector<Cochera> getCocheras() {
		return cocheras;
	}
	
	public CocheraView getView(){
		return new CocheraView(this.nroCochera , this.fila , this.piso , cocheras.get(1).getNroCochera() );
	}
	 

	
}
