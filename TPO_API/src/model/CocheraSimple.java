package model;

public class CocheraSimple extends Cochera {

	//Atributos heredados
	
	//Constructor
	public CocheraSimple(int nroCochera,int fila,int piso, boolean estado) {
		super();
		this.nroCochera = nroCochera;
		this.fila = fila;
		this.piso = piso;
		this.libre = estado;
	}
	
	public CocheraView getView(){
		return new CocheraView(this.nroCochera,this.fila,this.piso,-1);
	}
	
	
}
