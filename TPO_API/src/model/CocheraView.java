package model;


public class CocheraView {
	
	//Atributos
	private int nroCochera;
	private int fila;
	private int piso;
	private int extension;
	
	
	public CocheraView(int nroCochera, int fila, int piso, int extension) {
		super();
		this.nroCochera = nroCochera;
		this.fila = fila;
		this.piso = piso;
		this.extension = extension;
	}


	public int getNroCochera() {
		return nroCochera;
	}


	public int getFila() {
		return fila;
	}


	public int getPiso() {
		return piso;
	}


	public int getExtension() {
		return extension;
	}

	
	


	


}
