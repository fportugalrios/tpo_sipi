package model;

import java.sql.Timestamp;

import persistencia.AdmPersistenciaContrato;

public abstract class Contrato {
	
	//Atributos
	protected int nroContrato;
	protected Cliente cliente;
	protected Timestamp fechaDesde;
	protected Timestamp fechaHasta;
	protected Auto auto;
	protected Cochera cochera;
	protected Abono abono;
	protected boolean pago;
	protected boolean activo;
	
	//Getters & Setters
	public int getNroContrato() {
		return nroContrato;
	}
	
	public void setNroContrato(int nroContrato) {
		this.nroContrato = nroContrato;
	}
	
	public void setCliente(Cliente c){
		this.cliente = c;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public Timestamp getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Timestamp fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Timestamp getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Timestamp fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Auto getAuto() {
		return auto;
	}	

	public void setAuto(Auto auto) {
		this.auto = auto;
	}

	public Cochera getCochera() {
		return cochera;
	}

	public void setCochera(Cochera cochera) {
		this.cochera = cochera;
	}

	public Abono getAbono() {
		return abono;
	}

	public void setAbono(Abono abono) {
		this.abono = abono;
	}
	
	public void setPago(boolean pago){
		this.pago = pago;
	}
	public boolean estaPago(){
		return pago;
	}

	public boolean estaActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	//Metodos de persistencia
	public int insert(){
		return AdmPersistenciaContrato.getInstancia().insert(this);
	}
	
	public void update(){
		AdmPersistenciaContrato.getInstancia().update(this);
	}
	
	public void delete(){
		AdmPersistenciaContrato.getInstancia().delete(this);
	}
		
	public abstract ContratoView getContratoView();
	
	public abstract boolean sosEfectivo();
	
	public abstract boolean sosCheque();
	
	public abstract boolean sosTarjeta();
	
	public abstract boolean sosDebito();

}
