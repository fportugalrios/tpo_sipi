package model;

import java.sql.Timestamp;

public class ContratoCheque extends Contrato {
	
	//Atributos heredados
	private String nroCheque;
	private String banco;
	private Timestamp fechaVto;
	
	//Constructor
	public ContratoCheque(int nroContrato, Cliente cliente,Timestamp fechaDesde, Timestamp fechaHasta, Auto auto, Cochera cochera,
			Abono abono, String banco, String nroCheque, Timestamp fechaVto,boolean pago,boolean activo) {
		super();
		this.nroContrato = nroContrato;
		this.cliente = cliente;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.auto = auto;
		this.cochera = cochera;
		this.abono = abono;
		this.nroCheque = nroCheque;
		this.banco = banco;
		this.fechaVto = fechaVto;
		this.pago = pago;
		this.activo = activo;

	}
		
	//Getters & Setters
	public String getNroCheque() {
		return nroCheque;
	}

	public void setNroCheque(String nroCheque) {
		this.nroCheque = nroCheque;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}
	
	public Timestamp getFechaVto() {
		return fechaVto;
	}
	
	public void setFechaVto(Timestamp fechaVto) {
		this.fechaVto = fechaVto;
	}

	public ContratoView getContratoView(){
		CocheraView c = null;
		
		if (this.cochera != null)
			c = this.cochera.getView();
		
		//ORDEN: nroContrato,auto,banco,nroCheque,nroTarjeta,fechaVto,CBU,abono,activo
		return new ContratoView(nroContrato,cliente.getClienteView(),fechaDesde,fechaHasta,auto.getAutoView(),
				banco,nroCheque,0,fechaVto,c,abono.getAbonoView(),pago,activo);
	}
	
	public boolean sosEfectivo(){
		return false;
	}
	
	public boolean sosCheque(){
		return true;
	}
	
	public boolean sosTarjeta(){
		return false;
	}
	
	public boolean sosDebito(){
		return false;
	}
		

}
