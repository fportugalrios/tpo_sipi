package model;

import java.sql.Timestamp;

public class ContratoEfectivo extends Contrato {
	
	//Constructor
	public ContratoEfectivo(int nroContrato, Cliente cliente,Timestamp fechaDesde,Timestamp fechaHasta,Auto auto, Cochera cochera,Abono abono,boolean pago,boolean activo) {
		super();
		this.nroContrato = nroContrato;
		this.cliente = cliente;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.auto = auto;
		this.cochera = cochera;
		this.abono = abono;
		this.pago = pago;
		this.activo = activo;
	}


	public ContratoView getContratoView(){
		
		CocheraView c = null;
		
		if (this.cochera != null)
			c = this.cochera.getView();
		
		//ORDEN: nroContrato,auto,banco,nroCheque,nroTarjeta,fechaVto,CBU,abono,activo
		return new ContratoView(nroContrato,cliente.getClienteView(),fechaDesde,fechaHasta,	auto.getAutoView(),
				"","",0,null,c,abono.getAbonoView(),pago,activo);
	}
	
	public boolean sosEfectivo(){
		return true;
	}
	
	public boolean sosCheque(){
		return false;
	}
	
	public boolean sosTarjeta(){
		return false;
	}
	
	public boolean sosDebito(){
		return false;
	}
		

}
