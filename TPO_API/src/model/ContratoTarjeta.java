package model;

import java.sql.Timestamp;

public class ContratoTarjeta extends Contrato {
	
	//Atributos heredados
	private String banco;
	private int nroTarjeta;
	private Timestamp fechaVto;
	
	//Constructor
	public ContratoTarjeta(int nroContrato,Cliente cliente,Timestamp fechaDesde,Timestamp fechaHasta,Auto auto, Cochera cochera,Abono abono, String banco, 
			int nroTarjeta, Timestamp fechaVto,boolean pago,boolean activo) {
		super();
		this.nroContrato = nroContrato;
		this.cliente = cliente;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.auto = auto;
		this.cochera = cochera;
		this.abono = abono;
		this.nroTarjeta = nroTarjeta;
		this.banco = banco;
		this.fechaVto = fechaVto;
		this.pago = pago;
		this.activo = activo;
	}
	
	//Getters & Setters
	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public int getNroTarjeta() {
		return nroTarjeta;
	}

	public void setNroTarjeta(int nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}

	public Timestamp getFechaVto() {
		return fechaVto;
	}

	public void setFechaVto(Timestamp fechaVto) {
		this.fechaVto = fechaVto;
	}

	public ContratoView getContratoView(){
		
		CocheraView c = null;
		
		if (this.cochera != null)
			c = this.cochera.getView();
			
		return new ContratoView(nroContrato,cliente.getClienteView(),fechaDesde,fechaHasta,
				auto.getAutoView(),banco,"",nroTarjeta,fechaVto,c,abono.getAbonoView(),pago,activo);
	}	

	public boolean sosEfectivo(){
		return false;
	}
	
	public boolean sosCheque(){
		return false;
	}
	
	public boolean sosTarjeta(){
		return true;
	}
	
	public boolean sosDebito(){
		return false;
	}	

}
