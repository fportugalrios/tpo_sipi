package model;

import java.sql.Timestamp;


public class ContratoView {
	
	//Atributos
	private int nroContrato;
	private ClienteView cliente;
	protected Timestamp fechaDesde;
	protected Timestamp fechaHasta;
	private AutoView auto;
	private String banco;
	private String nroCheque;
	private int nroTarjeta;
	private Timestamp fechaVto;
	private CocheraView cochera;
	private AbonoView abono;
	private boolean pago;
	private boolean activo;
	
	//Constructor
	public ContratoView(int nroContrato, ClienteView cliente, Timestamp fechaDesde, Timestamp fechaHasta,
			AutoView auto, String banco,String nroCheque, int nroTarjeta, Timestamp fechaVto, CocheraView cochera,
			AbonoView abono, boolean pago,boolean activo) {
		this.nroContrato = nroContrato;
		this.cliente = cliente;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.auto = auto;
		this.banco = banco;
		this.nroCheque = nroCheque;
		this.nroTarjeta = nroTarjeta;
		this.fechaVto = fechaVto;
		this.cochera = cochera;
		this.abono = abono;
		this.pago = pago;
		this.activo = activo;
	}

	public int getNroContrato() {
		return nroContrato;
	}

	public AutoView getAuto() {
		return auto;
	}

	public String getBanco() {
		return banco;
	}

	public String getNroCheque() {
		return nroCheque;
	}

	public int getNroTarjeta() {
		return nroTarjeta;
	}

	public Timestamp getFechaVto() {
		return fechaVto;
	}

	public boolean estaPago() {
		return pago;
	}

	public AbonoView getAbono() {
		return abono;
	}

	public boolean isActivo() {
		return activo;
	}

	public ClienteView getCliente() {
		return cliente;
	}

	public Timestamp getFechaDesde() {
		return fechaDesde;
	}

	public Timestamp getFechaHasta() {
		return fechaHasta;
	}	
	
	public CocheraView getCochera(){
		return this.cochera;
	}
	
	

}
