package model;

import java.util.Vector;

import persistencia.AdmPersistenciaCochera;

public class Mapa {
		
	//Atributos
	private Vector<Cochera> cocheras;

	//Constructor
	public Mapa() {		
		this.cocheras = AdmPersistenciaCochera.getInstancia().selectAll();
	}

	
	public Vector<Cochera> getCocheras() {
		return cocheras;
	}
	
	public void eliminarCocheras(){
		cocheras.removeAllElements();
	}
	
	//Calcula la cant de cocheras simples libres. Con este dato puedo saber cuantas cocheras dobles puedo crear.
	public int getCantCocherasLibres(){
		int cant = 0;
		
		for (Cochera c : cocheras)
			if ( c.estaLibre() == true )
				cant++;
		
		return cant;					
	}
		
	//Obtiene una cochera simple libre y cambia su estado  a ocupada.
	public CocheraSimple getCocheraSimplelLibre(Abono abono){
			
		if (abono.getHoras() <= 24){
			
			for (Cochera cochera : cocheras){
				
				if ( cochera.estaLibre() ){
					cochera.setLibre(false);
					
					//Actualizo su estado en la BD
					cochera.updateEstado();
					
					return (CocheraSimple)cochera;
				}
					
			}
						
		}
		else{
			
			for (int i = cocheras.size()-1 ; i >= 0 ; i--){
				
				Cochera cochera = cocheras.get(i);
				
				if ( cochera.estaLibre() ){
					cochera.setLibre(false);
					
					//Actualizo su estado en la BD
					cochera.updateEstado();
					
					return (CocheraSimple)cochera;
				}
					
			}
			
		}
			
		return null;
		
	}
	
	
	//Crea y devuelve una cochera doble compuesta por dos cocheras simples libres y cambia su estado a ocupada. 
	//Luego las cocheras simples libres son eliminadas del vector cocheras 
	public CocheraDoble getCocheraDobleLibre(Abono abono){
		
		CocheraDoble doble1 = null;
		CocheraDoble doble2 = null;
		CocheraSimple c1 = null;
		CocheraSimple c2 = null;
		boolean encontrado = false;
		
		if ( getCantCocherasLibres() >= 2 ){
			
			if (abono.getHoras() <= 24){
				
				//Busco 2 cocheras libres
				for (int i=0 ; i < cocheras.size() ; i++){
					
					if ( (i+1 < cocheras.size()) ){
						
						if ( cocheras.get(i).estaLibre() && cocheras.get(i+1).estaLibre() ){
							
							c1 = (CocheraSimple)cocheras.get(i);
							c2 = (CocheraSimple)cocheras.get(i+1);
							
							if ( (c1.getFila() == c2.getFila()) && (c1.getPiso()==c2.getPiso()) ){
								encontrado = true;
								break;
							}
						}	
					}				
				}				
			}
			else{
				
				//Busco 2 cocheras libres
				for (int i= cocheras.size()-1 ; i >= 0 ; i--){
					
					if ( i-1 > 0 ){
						
						if ( cocheras.get(i).estaLibre() && cocheras.get(i-1).estaLibre() ){
							
							c1 = (CocheraSimple)cocheras.get(i);
							c2 = (CocheraSimple)cocheras.get(i-1);
							
							if ( (c1.getFila() == c2.getFila()) && (c1.getPiso()==c2.getPiso()) ){
								encontrado = true;
								break;
							}
						}
					}				
				}
				
			}
			
			if (encontrado){
				
				//Las entrecruzo en la BD
				c1.updateDoble(c2.getNroCochera());
				c2.updateDoble(c1.getNroCochera());
				
				//Creo la doble
				doble1 = new CocheraDoble(c1,c2,false);
				doble2 = new CocheraDoble(c2,c1,false);
				
				//Actualizo mapa
				cocheras.set(c1.getNroCochera()-1, doble1);
				cocheras.set(c2.getNroCochera()-1, doble2);		
			}

		}
		
		return doble1;
				
	}
	
	//Verifica si una cochera es simple o doble
	private boolean esDoble(Cochera cochera){		
		try{			
			CocheraDoble c = (CocheraDoble)cochera;
			
			if ( c.getCocheras().isEmpty() )
				return false;
			else
				return true;
		}
		catch (Exception e){
			return false;
		}
	}

	//Libera una determinada cochera y pone su estado en libre
	//Si es simple entonces la libera. Si es doble la descompone en dos simples liberando cada una.
	public void liberarCochera(int nroCochera){
		
		int i = 0;
		boolean encontrado = false;
		
		while (i<cocheras.size() && encontrado != true){

			if ( cocheras.elementAt(i).getNroCochera() == nroCochera ){				
				encontrado = true;
				
				 if ( !esDoble(cocheras.elementAt(i)) ){
					 cocheras.elementAt(i).setLibre(true);
					 cocheras.elementAt(i).updateEstado();
				 }
				 else{					 
					 CocheraDoble d = (CocheraDoble)cocheras.elementAt(i);
					 CocheraSimple c1 = (CocheraSimple)d.getCocheras().elementAt(0);
					 CocheraSimple c2 = (CocheraSimple)d.getCocheras().elementAt(1);
					 
					 //Actualizo su estado a libre en memoria y en BD
					 c1.setLibre(true);
					 c2.setLibre(true);
					 c1.updateEstado();
					 c2.updateEstado();
					 
					 //Desasocio en la BD
					 c1.updateDoble(-1);
					 c2.updateDoble(-1);
					 
					 //Actualizo mapa
					 cocheras.set(c1.getNroCochera()-1, c1);
					 cocheras.set(c2.getNroCochera()-1, c2);
					 
				 }//fin if doble
				 
			}//fin if
			
			i++;
		
	
		}//fin while

	
	}
	
	private void mostrar(){
		
		for (Cochera c: cocheras){
			if ( !esDoble(c) )
				System.out.println(c.getNroCochera() + " - Simple");
			else
				System.out.println(c.getNroCochera() + " - Doble");
			
		}
		
	}
		

}
