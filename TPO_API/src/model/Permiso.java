package model;

public enum Permiso {
	
	Master,
	Administrador,
	Operador,
	Ninguno;

}
