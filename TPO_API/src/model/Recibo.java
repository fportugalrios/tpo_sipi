package model;

import java.sql.Timestamp;

public class Recibo {
	
	//Atributos
	private int nroRecibo;
	private Timestamp fecha;
	private Cliente cliente;
	private Abono abono;	
	private float cantHoras;
	private float total;
		
	public Recibo(int nroRecibo, Timestamp fecha, Cliente cliente, Abono abono, float cantHoras, float total) {
		super();
		this.nroRecibo = nroRecibo;
		this.fecha = fecha;
		this.cliente = cliente;
		this.abono = abono;
		this.cantHoras = cantHoras;
		this.total = total;
	}


	public int getNroRecibo() {
		return nroRecibo;
	}

	public void setNroRecibo(int nroRecibo) {
		this.nroRecibo = nroRecibo;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Abono getAbono() {
		return abono;
	}

	public void setAbono(Abono abono) {
		this.abono = abono;
	}

	public float getCantHoras() {
		return cantHoras;
	}

	public void setCantHoras(float cantHoras) {
		this.cantHoras = cantHoras;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public ReciboView toView(){
		return new ReciboView(nroRecibo, fecha, cliente.getClienteView() , abono.getAbonoView() , cantHoras, total);
	}
	
	
	
	
	

}
