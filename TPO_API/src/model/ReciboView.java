package model;

import java.sql.Timestamp;

public class ReciboView {
	
	//Atributos
	private int nroRecibo;
	private Timestamp fecha;
	private ClienteView cliente;
	private AbonoView abono;	
	private float cantHoras;
	private float total;
		
	public ReciboView(int nroRecibo, Timestamp fecha, ClienteView cliente, AbonoView abono, float cantHoras, float total) {
		super();
		this.nroRecibo = nroRecibo;
		this.fecha = fecha;
		this.cliente = cliente;
		this.abono = abono;
		this.cantHoras = cantHoras;
		this.total = total;
	}


	public int getNroRecibo() {
		return nroRecibo;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public ClienteView getCliente() {
		return cliente;
	}

	public AbonoView getAbono() {
		return abono;
	}

	public float getCantHoras() {
		return cantHoras;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}
}
