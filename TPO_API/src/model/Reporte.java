package model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;



public class Reporte {
	
	//Atributos
	private Timestamp fechaDesde;
	private Timestamp fechaHasta;
	private List<Recibo> recibos;
	private float total;
	
	public Reporte(Timestamp fechaDesde, Timestamp fechaHasta, List<Recibo> recibos) {
		super();
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.recibos = recibos;
		this.total = 0;
		
		if (recibos != null){			
			
			for (Recibo f : recibos)
				this.total = this.total + f.getTotal();			
		}
		
	}

	public Timestamp getFechaDesde() {
		return fechaDesde;
	}

	public Timestamp getFechaHasta() {
		return fechaHasta;
	}

	public List<Recibo> getRecibos() {
		return recibos;
	}

	public float getTotal() {
		return total;
	}
	
	public ReporteView toView(){
		
		List<ReciboView> fv = new ArrayList<ReciboView>();
		
		for (Recibo f : recibos)
			fv.add( f.toView() );
		
		return new ReporteView(fechaDesde,fechaHasta,fv);
	}
		
	

}
