package model;

import java.sql.Timestamp;
import java.util.List;


public class ReporteView {
	
	//Atributos
	private Timestamp fechaDesde;
	private Timestamp fechaHasta;
	private List<ReciboView> recibos;
	private float total;
	
	public ReporteView(Timestamp fechaDesde, Timestamp fechaHasta, List<ReciboView> recibos) {
		super();
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.recibos = recibos;
		this.total = 0;
		
		if (recibos != null){			
			
			for (ReciboView f : recibos)
				this.total = this.total + f.getTotal();			
		}
		
	}

	public Timestamp getFechaDesde() {
		return fechaDesde;
	}

	public Timestamp getFechaHasta() {
		return fechaHasta;
	}

	public List<ReciboView> getRecibos() {
		return recibos;
	}

	public float getTotal() {
		return total;
	}
	
	
		
	

}
