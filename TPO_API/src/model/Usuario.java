package model;

import persistencia.AdmPersistenciaUsuario;

public class Usuario {
	
	//Atributos
	private String alias;
	private String password;
	private String nombreReal;
	private Permiso permiso;
	private boolean activo;
	
	public Usuario(String alias, String password, String nombreReal,Permiso permiso, boolean activo) {
		super();
		this.alias = alias;
		this.password = password;
		this.nombreReal = nombreReal;
		this.permiso = permiso;
		this.activo = activo;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombreReal() {
		return nombreReal;
	}

	public void setNombreReal(String nombreReal) {
		this.nombreReal = nombreReal;
	}

	public Permiso getPermiso() {
		return permiso;
	}

	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}

	public boolean estaActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	public UsuarioView getView(){
		return new UsuarioView(alias,password,nombreReal,permiso,activo);
	}
	
	public void insert(){
		AdmPersistenciaUsuario.getInstancia().insert(this);
	}
	
	public void delete(){
		AdmPersistenciaUsuario.getInstancia().delete(this);
	}
	

}
