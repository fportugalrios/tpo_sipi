package model;

public class UsuarioView {
	
	//Atributos
	private String alias;
	private String password;
	private String nombreReal;
	private Permiso permiso;
	private boolean activo;
	
	public UsuarioView(String alias, String password, String nombreReal,Permiso permiso, boolean activo) {
		super();
		this.alias = alias;
		this.password = password;
		this.nombreReal = nombreReal;
		this.permiso = permiso;
		this.activo = activo;
	}

	public String getAlias() {
		return alias;
	}

	public String getPassword() {
		return password;
	}

	public String getNombreReal() {
		return nombreReal;
	}

	public Permiso getPermiso() {
		return permiso;
	}

	public boolean isActivo() {
		return activo;
	}

	
	
	
	
	

}
