package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import model.*;


public class AdmPersistenciaAbono extends AdministradorPersistencia{
	
	//Atributos
	private static AdmPersistenciaAbono instancia;
	private String sentenciaSelectAll = "select * from SIPI.dbo.abonos where activo = 'true'";
	private String sentenciaSelect = "select * from SIPI.dbo.abonos where nombre = ? AND activo = 'true'";
	private String sentenciaSelectAB = "select * from SIPI.dbo.abonos where nombre = ?";
	private String sentenciaUpdate = "update SIPI.dbo.abonos set horas = ?,precio = ?,descuento = ?,activo=? where nombre = ?";
	private String sentenciaDelete = "update SIPI.dbo.abonos set activo='false' where nombre = ?";
	private String sentenciaInsert = "insert into SIPI.dbo.abonos values (?,?,?,?,?)";
	
	//Constructror
	private AdmPersistenciaAbono(){		
	}
	
	public static AdmPersistenciaAbono getInstancia(){
		if (instancia == null)
			instancia = new AdmPersistenciaAbono();
		return instancia;
	}

	//SELECT
	public Abono select(String nombreAbono){
		try	{
			Abono abono = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelect);
			s.setString(1,nombreAbono);
			
			ResultSet result = s.executeQuery();
			
			//Devuelve toda la tabla en un cursor. Recorro el vector cursor ResultSet
			while (result.next()){
				String nombre = result.getString(1).trim();
				int horas = result.getInt(2);
				float precio = result.getFloat(3);
				float descuento = result.getFloat(4);				
				
				abono = new Abono(nombre,horas,precio,descuento);
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return abono;
		}
		catch (Exception e)	{
			System.out.println("Error en select abono " + e );
			return null;
		}
		
	}
	
	public Abono selectAB(String nombreAbono){
		try	{
			Abono abono = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelectAB);
			s.setString(1,nombreAbono);
			
			ResultSet result = s.executeQuery();
			
			//Devuelve toda la tabla en un cursor. Recorro el vector cursor ResultSet
			while (result.next()){
				String nombre = result.getString(1).trim();
				int horas = result.getInt(2);
				float precio = result.getFloat(3);
				float descuento = result.getFloat(4);
				boolean activo = Boolean.parseBoolean( result.getString(5).trim() );
				
				abono = new Abono(nombre,horas,precio,descuento,activo);
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return abono;
		}
		catch (Exception e)	{
			System.out.println("Error en select abono " + e );
			return null;
		}
		
	}
	
	public Vector<Abono> selectAll(){
		try	{
			
			Vector<Abono> abonos = new Vector<Abono>();
			
			Connection c = PoolConnection.getPoolConnection().getConnection();
			Statement s = c.createStatement();
			ResultSet result = s.executeQuery(sentenciaSelectAll);
			
			while (result.next()){
				
				String nombre = result.getString(1).trim();
				int horas = result.getInt(2);
				float precio = result.getFloat(3);
				float descuento = result.getFloat(4); 
				
				abonos.add(new Abono(nombre,horas,precio,descuento));
				
			}
			PoolConnection.getPoolConnection().realeaseConnection(c);
			return abonos;
		}
		catch(Exception e){
			System.out.println("Error en selectAll abono " + e );
			return null;
		}
		
	}
	
	//INSERT
	public void insert(Object o) {
		
		try	{
			
			Abono abono = (Abono)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaInsert);

			s.setString(1, abono.getNombre());
			s.setInt(2, abono.getHoras());
			s.setFloat(3, abono.getPrecio());
			s.setFloat(4, abono.getDescuento());
			s.setString(5, "true");
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e){
			System.out.println("Error en insert abono " + e );
		}
	}


	//UPDATE
	public void update(Object o){
		
		try
		{
			Abono abono = (Abono)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaUpdate);

			s.setInt(1, abono.getHoras() );
			s.setFloat(2, abono.getPrecio() );
			s.setFloat(3, abono.getDescuento() );
			s.setString(4, Boolean.toString( abono.estaActivo()) ); 
			s.setString(5, abono.getNombre() );
			
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en update abono: " + e );
		}
		
	}
			
	
	//DELETE
	public void delete(Object o){
		
		try{
			Abono abono= (Abono)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaDelete);
			s.setString(1, abono.getNombre());
			s.execute();
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e){
			System.out.println("Error en delete abono");
		}
	}


}
