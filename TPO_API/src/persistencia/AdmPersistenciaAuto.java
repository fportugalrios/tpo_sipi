package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.*;


public class AdmPersistenciaAuto extends AdministradorPersistencia{
	
	//Atributos
	private static AdmPersistenciaAuto instancia;
	private String sentenciaSelect = "select * from SIPI.dbo.autos where patente = ? AND activo = 'true'";
	private String sentenciaSelectAllMarcas = "select * from SIPI.dbo.marcas";
	private String sentenciaSelectAB = "select * from SIPI.dbo.autos where patente = ?";
	private String sentenciaInsert = "insert into SIPI.dbo.autos values (?,?,?,?,?)";
	private String sentenciaUpdate = "update SIPI.dbo.autos set marca = ? ,modelo = ? ,tipo = ?,activo=? where patente = ?";
	private String sentenciaDelete = "update SIPI.dbo.autos set activo = 'false' where patente = ?";

	//Constructor
	private AdmPersistenciaAuto(){		
	}
	
	public static AdmPersistenciaAuto getInstancia(){
		if (instancia == null)
			instancia = new AdmPersistenciaAuto();
		return instancia;
	}
	
	//SELECT
	public Auto select(String patente){
		try	{
			Auto auto = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelect);
			s.setString(1,patente);
			
			ResultSet result = s.executeQuery();
			
			//Devuelve toda la tabla en un cursor. Recorro el vector cursor ResultSet
			while (result.next()){
				
				String p = result.getString(1).trim();
				String marca = result.getString(2).trim();
				String modelo = result.getString(3).trim();
				String tipo = result.getString(4).trim();				
				
				auto = new Auto(p,marca,modelo,tipo);
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return auto;
		}
		catch (Exception e)	{
			System.out.println("Error en select auto");
		}
		return null;
	}
	
	public Auto selectAB(String patente){
		try	{
			Auto auto = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelectAB);
			s.setString(1,patente);
			
			ResultSet result = s.executeQuery();
			
			//Devuelve toda la tabla en un cursor. Recorro el vector cursor ResultSet
			while (result.next()){
				
				String p = result.getString(1).trim();
				String marca = result.getString(2).trim();
				String modelo = result.getString(3).trim();
				String tipo = result.getString(4).trim();
				boolean activo = Boolean.parseBoolean( result.getString(5).trim() );
				
				auto = new Auto(p,marca,modelo,tipo,activo);
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return auto;
		}
		catch (Exception e)	{
			System.out.println("Error en selectAB auto");
		}
		return null;
	}
	
	public List<String> selectAll(){
		try	{
			
			List<String> lista = new ArrayList<String>();
			
			Connection c = PoolConnection.getPoolConnection().getConnection();
			Statement s = c.createStatement();
			ResultSet result = s.executeQuery(sentenciaSelectAllMarcas);
			
			while (result.next()){
				
				int id = result.getInt(1);
				String marca = result.getString(2).trim();
						
				lista.add(marca);
				
			}
			PoolConnection.getPoolConnection().realeaseConnection(c);
			return lista;
		}
		catch(Exception e)	{
			System.out.println("Error en selectAllMarcas Auto");
			return null;
			
		}
		
	}
	
	//INSERT
	public void insert(Object o) {
		
		try	{
			
			Auto auto = (Auto)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaInsert);

			s.setString(1, auto.getPatente());
			s.setString(2, auto.getMarca());
			s.setString(3, auto.getModelo());
			s.setString(4, auto.getTipo());
			s.setString(5, "true");
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en insert auto");
		}
	}


	//UPDATE
	public void update(Object o){
		
		try
		{
			Auto auto = (Auto)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaUpdate);
			
			s.setString(1, auto.getMarca());
			s.setString(2, auto.getModelo());
			s.setString(3, auto.getTipo());			
			s.setString(4, Boolean.toString(auto.estaActivo()) );
			
			s.setString(5, auto.getPatente());
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en update auto");
		}
		
	}
			
	
	//DELETE
	public void delete(Object o){
		
		try{
			Auto auto = (Auto)o;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaDelete);
		
			s.setString(1, auto.getPatente());
			
			s.execute();
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en delete auto");
		}
	}

}
