package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import model.*;


public class AdmPersistenciaBanco extends AdministradorPersistencia{
	
	//Atributos
	private static AdmPersistenciaBanco instancia;
	private String sentenciaSelectAll = "select * from SIPI.dbo.bancos where activo = 'true'";
	private String sentenciaSelect = "select * from SIPI.dbo.bancos where banco = ? AND activo = 'true'";
	private String sentenciaSelectAB = "select * from SIPI.dbo.bancos where banco = ?";
	private String sentenciaUpdate = "update SIPI.dbo.bancos set url = ? ,archivo = ?,activo=? where banco = ?";
	private String sentenciaDelete = "update SIPI.dbo.bancos set activo='false' where banco = ?";
	private String sentenciaInsert = "insert into SIPI.dbo.bancos values (?,?,?,?)";
	
	//Constructor
	private AdmPersistenciaBanco(){		
	}
	
	public static AdmPersistenciaBanco getInstancia(){
		if (instancia == null)
			instancia = new AdmPersistenciaBanco();
		return instancia;
	}
	
	//SELECT
	public Banco select(String banco){
		try	{
			
			Banco medio = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelect);
			s.setString(1,banco);
			
			ResultSet result = s.executeQuery();
			
			while (result.next()){
				
				String url = result.getString(2).trim();
				String archivo = result.getString(3).trim();
				
				medio = new Banco(banco,url,archivo);
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return medio;
		}
		catch (Exception e)	{
			System.out.println("Error en select medios de pago");
			return null;
		}
		
	}
	
	public Banco selectAB(String banco){
		try	{
			
			Banco medio = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelectAB);
			s.setString(1,banco);
			
			ResultSet result = s.executeQuery();
			
			while (result.next()){
				
				String b = result.getString(1).trim();
				String url = result.getString(2).trim();
				String archivo = result.getString(3).trim();
				boolean activo = Boolean.parseBoolean( result.getString(4).trim() );
				
				medio = new Banco(b,url,archivo,activo);
				
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return medio;
		}
		catch (Exception e)	{
			System.out.println("Error en selectAB medios de pago");
			return null;
		}
		
	}
	
	public Vector<Banco> selectAll(){
		try	{
			
			Vector<Banco> mediosPago = new Vector<Banco>();
			
			Connection c = PoolConnection.getPoolConnection().getConnection();
			Statement s = c.createStatement();
			ResultSet result = s.executeQuery(sentenciaSelectAll);
			
			while (result.next()){
								
				String banco = result.getString(1).trim();
				String url = result.getString(2).trim();
				String archivo = result.getString(3).trim();
				
				Banco medio = new Banco(banco,url,archivo);

				
				mediosPago.add(medio);
				
			}
			PoolConnection.getPoolConnection().realeaseConnection(c);
			return mediosPago;
		}
		catch(Exception e)	{
			System.out.println("Error en selectAll medios");
		}
		return null;
	}
	
	//INSERT
	public void insert(Object o) {
		
		try	{
			
			Banco medio = (Banco)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaInsert);

			s.setString(1, medio.getBanco());
			s.setString(2, medio.getUrlConexion());
			s.setString(3, medio.getNombreArchivo());
			s.setString(4, "true");
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en insert medios");
		}
	}


	//UPDATE
	public void update(Object o){
		
		try
		{
			Banco medio = (Banco)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaUpdate);
			
			s.setString(1, medio.getUrlConexion());
			s.setString(2, medio.getNombreArchivo());
			s.setString(3, Boolean.toString(medio.estaActivo()) );
			
			s.setString(4, medio.getBanco());
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en update medios");
		}
		
	}
			
	
	//DELETE
	public void delete(Object o){
		
		try{
			Banco medio= (Banco)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaDelete);
			s.setString(1, medio.getBanco() );
			s.execute();
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en delete medios");
		}
	}


}
