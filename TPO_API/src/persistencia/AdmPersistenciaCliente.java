package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import model.*;


public class AdmPersistenciaCliente extends AdministradorPersistencia{
	
	//Atributos
	private static AdmPersistenciaCliente instancia;
	private String sentenciaSelectAll = "select * from SIPI.dbo.clientes where activo = 'true'";
	private String sentenciaSelect = "select * from SIPI.dbo.clientes where dni = ? AND activo = 'true'";
	private String sentenciaSelectAB = "select * from SIPI.dbo.clientes where dni = ?";
	private String sentenciaUpdate = "update SIPI.dbo.clientes set nombre = ?,domicilio = ?,telefono = ?,mail = ?,activo=? where dni = ?";
	private String sentenciaDelete = "update SIPI.dbo.clientes set activo='false' where dni = ?";
	private String sentenciaInsert = "insert into SIPI.dbo.clientes values (?,?,?,?,?,?)";
	
	
	//Constructor
	private AdmPersistenciaCliente(){		
	}
	
	public static AdmPersistenciaCliente getInstancia(){
		if (instancia == null)
			instancia = new AdmPersistenciaCliente();
		return instancia;
	}


	//SELECT
	public Cliente select(int dni){
		try	{
			
			Cliente cliente = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelect);
			s.setInt(1,dni);
			
			ResultSet result = s.executeQuery();
			
			//Devuelve toda la tabla en un cursor. Recorro el vector cursor ResultSet
			while (result.next()){
				
				int d = result.getInt(1);
				String nombre = result.getString(2).trim();
				String domicilio = result.getString(3).trim();
				int telefono = result.getInt(4);
				String mail = result.getString(5).trim();
				
				cliente = new Cliente(d,nombre,domicilio,telefono,mail);			
				
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return cliente;
		}
		catch (Exception e)		{
			System.out.println("Error en select clientes");
			return null;
		}
		
	}
	
	public Cliente selectAB(int dni){
		try	{
			
			Cliente cliente = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelectAB);
			s.setInt(1,dni);
			
			ResultSet result = s.executeQuery();
			
			//Devuelve toda la tabla en un cursor. Recorro el vector cursor ResultSet
			while (result.next()){
				
				int d = result.getInt(1);
				String nombre = result.getString(2).trim();
				String domicilio = result.getString(3).trim();
				int telefono = result.getInt(4);
				String mail = result.getString(5).trim();
				boolean activo = Boolean.parseBoolean(result.getString(6).trim() );				
				
				cliente = new Cliente(d,nombre,domicilio,telefono,mail,activo);			
				
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return cliente;
		}
		catch (Exception e)		{
			System.out.println("Error en selectAB clientes");
			return null;
		}		
	}
	
	public Vector<Cliente> selectAll(){
		try	{
			
			Vector<Cliente> clientes = new Vector<Cliente>();
			
			Connection c = PoolConnection.getPoolConnection().getConnection();
			Statement s = c.createStatement();
			ResultSet result = s.executeQuery(sentenciaSelectAll);
			
			while (result.next()){
				
				int dni = result.getInt(1);
				String nombre = result.getString(2).trim();
				String domicilio = result.getString(3).trim();
				int telefono = result.getInt(4);
				String mail = result.getString(5).trim();
				
				Cliente cliente = new Cliente(dni,nombre,domicilio,telefono,mail);
				
				clientes.add(cliente);
				
			}
			PoolConnection.getPoolConnection().realeaseConnection(c);
			return clientes;
		}
		catch(Exception e)	{
			System.out.println("Error en selectAll clientes");
			return null;
			
		}
		
	}
	
	//INSERT
	public void insert(Object o) {
		
		try	{
			
			Cliente cliente = (Cliente)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaInsert);

			s.setInt(1, cliente.getDNI());
			s.setString(2, cliente.getNombre());
			s.setString(3, cliente.getDomicilio());
			s.setInt(4, cliente.getTel());
			s.setString(5, cliente.getMail());
			s.setString(6, "true");
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en insert clientes");
		}
	}


	//UPDATE
	public void update(Object o){
		
		try
		{
			Cliente cliente = (Cliente)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaUpdate);

			s.setString(1, cliente.getNombre());
			s.setString(2, cliente.getDomicilio());
			s.setInt(3, cliente.getTel());
			s.setString(4, cliente.getMail());
			s.setString(5, Boolean.toString(cliente.estaActivo()) ); 
			
			s.setInt(6, cliente.getDNI());
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)		{
			System.out.println("Error en update clientes");
		}
		
	}
			
	
	//DELETE
	public void delete(Object o){
		
		try{
			Cliente cliente = (Cliente)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaDelete);
			s.setInt(1, cliente.getDNI());
			s.execute();
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en delete clientes");
		}
	}


}
