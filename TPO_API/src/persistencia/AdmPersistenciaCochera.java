package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import model.*;


public class AdmPersistenciaCochera extends AdministradorPersistencia{
	
	//Atributos
	private static AdmPersistenciaCochera instancia;
	private String sentenciaSelectAll = "select * from SIPI.dbo.cocheras";
	private String sentenciaSelect = "select * from SIPI.dbo.cocheras where nroCochera = ?";
	private String sentenciaUpdateEstado = "update SIPI.dbo.cocheras set libre = ? where nroCochera = ?";
	private String sentenciaUpdateDoble = "update SIPI.dbo.cocheras set tipo = ?, extension = ?, libre = ? where nroCochera = ?";
	private String sentenciaInsert = "insert into SIPI.dbo.cocheras values (?,?,?,?,?)";
	
	private AdmPersistenciaCochera(){		
	}
	
	public static AdmPersistenciaCochera getInstancia(){
		if (instancia == null)
			instancia = new AdmPersistenciaCochera();
		return instancia;
	}

	//SELECT
	public Cochera select(int c){
		try	{
			
			Cochera cochera = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelect);
			s.setInt(1,c);
			
			ResultSet result = s.executeQuery();
			
			while (result.next()){
				
				int nroCochera = result.getInt(1);
				int fila = result.getInt(2);
				int piso = result.getInt(3);
				String tipo = result.getString(4).trim();
				int extension = result.getInt(5);
				String libre = result.getString(6).trim();
				
				if ( tipo.equals("simple") )
					cochera = new CocheraSimple(nroCochera, fila, piso, Boolean.parseBoolean(libre) );
				else{
					CocheraSimple c1 = new CocheraSimple(nroCochera,fila,piso, Boolean.valueOf(libre) );
					CocheraSimple c2 = new CocheraSimple(extension,fila,piso, Boolean.valueOf(libre) );
					cochera = new CocheraDoble(c1,c2,false);
				}		
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return cochera;
		}
		catch (Exception e)	{
			System.out.println("Error en select de cocheras2");
		}
		return null;
	}
	
	public Vector<Cochera> selectAll(){
		try	{
			
			Vector<Cochera> cocheras = new Vector<Cochera>();
			Cochera cochera = null;
			
			Connection c = PoolConnection.getPoolConnection().getConnection();
			Statement s = c.createStatement();
			ResultSet result;
		
			result = s.executeQuery(sentenciaSelectAll);

			while (result.next()){
				
				int nroCochera = result.getInt(1);
				int fila = result.getInt(2);
				int piso = result.getInt(3);
				String tipo = result.getString(4).trim();
				int extension = result.getInt(5);
				String libre = result.getString(6).trim();

				if ( tipo.equals("simple") ){ 
					cochera = new CocheraSimple(nroCochera, fila, piso, Boolean.parseBoolean(libre) );
				}
				else{
					CocheraSimple c1 = new CocheraSimple(nroCochera,fila,piso, Boolean.valueOf(libre) );
					CocheraSimple c2 = new CocheraSimple(extension,fila,piso, Boolean.valueOf(libre) );
					cochera = new CocheraDoble(c1,c2,false);
				}
					
				cocheras.add(cochera);
	
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(c);
			return cocheras;
		}
		catch(Exception e)	{
			
			System.out.println("Error en selectAll cocheras2 " + e.getMessage());
			
		}
		return null;
	}
	
	
	//INSERT
	public void insert(Object o) {
		
		try	{
			
			Cochera cochera = (Cochera)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaInsert);
			
			s.setInt(1,  cochera.getFila() );
			s.setInt(2,  cochera.getPiso() );
			s.setString(3, "simple");
			s.setInt(4,  -1 );
			s.setString(5, String.valueOf(cochera.estaLibre()));
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en insert cochera2");
		}
	}



	//UPDATE
	public void update(Object o){
		
		try	{
			
			Cochera cochera = (Cochera)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaUpdateEstado);
			
			s.setString(1, String.valueOf(cochera.estaLibre()) );
			s.setInt(2, cochera.getNroCochera() );
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)		{
			System.out.println("Error en updateEstado cocheras2");
		}
		
	}
	
	
	public void updateDoble(Object o,int extension){
		
		try	{
			// fportugal
			Cochera cochera = (Cochera)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaUpdateDoble);
			
			if (extension != -1){ 
				
				//Creo una doble				
				s.setString(1, "doble" );
				s.setInt(2, extension);
				s.setString(3, "false" );
				s.setInt(4, cochera.getNroCochera() );
				
			}
			else{
				
				//Desasocio una doble
				s.setString(1, "simple" );
				s.setInt(2, -1);
				s.setString(3, "true" );
				s.setInt(4, cochera.getNroCochera() );
				
			}
			
			s.execute();			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			
			
		}
		catch (Exception e)		{
			System.out.println("Error en updateDoble cocheras2");
		}
		
	}
			

	public void delete(Object d) {
		// El sistema no borra cocheras
		
	}

}
