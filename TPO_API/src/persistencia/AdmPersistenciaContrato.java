package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Vector;

import model.*;


public class AdmPersistenciaContrato {
	
	//Atributos
	private static AdmPersistenciaContrato instancia;
	private String sentenciaSelectAll = "select * from SIPI.dbo.contratos where activo = 'true'";
	private String sentenciaSelect = "select * from SIPI.dbo.contratos where nroContrato = ? AND activo = 'true'";
	private String sentenciaSelectPatente = "select * from SIPI.dbo.contratos where patente = ? AND activo = 'true'";
	private String sentenciaInsert = "insert into SIPI.dbo.contratos values (?,?,?,?,?,?,?,?,?,?,?,?)";
	private String sentenciaUpdate = "update SIPI.dbo.contratos set nroCochera = ? , fechaHasta = ? , pago = ? where nroContrato = ?";
	private String sentenciaDelete = "update SIPI.dbo.contratos set activo = 'false' where nroContrato = ?";
	
	
	private AdmPersistenciaContrato(){		
	}
	
	public static AdmPersistenciaContrato getInstancia(){
		if (instancia == null)
			instancia = new AdmPersistenciaContrato();
		return instancia;
	}

	//SELECT
	public Contrato select(int nroContrato){
		try	{
			Contrato contrato = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelect);
			s.setInt(1,nroContrato);
			
			ResultSet result = s.executeQuery();
			
			while (result.next()){
				
				int dni = result.getInt(2);
				Timestamp fechaDesde = result.getTimestamp(3);
				Timestamp fechaHasta = result.getTimestamp(4);
				String patente = result.getString(5).trim();
				int nroCochera = result.getInt(6);
				String nombreAbono = result.getString(7).trim();
				String banco = (result.getObject(8) == null) ? "" : result.getString(8).trim();
				String nroCheque = (result.getObject(9) == null) ? "" : result.getString(9).trim();
				int nroTarjeta = (result.getObject(10) == null) ? 0 : result.getInt(10);
				Timestamp fechaVto = result.getTimestamp(11);
				boolean pago = Boolean.parseBoolean(result.getString(12).trim());
				boolean activo = Boolean.parseBoolean(result.getString(13).trim());
								
				Cliente cliente = AdmPersistenciaCliente.getInstancia().select(dni);
				Auto auto = AdmPersistenciaAuto.getInstancia().select(patente);
				Cochera cochera = AdmPersistenciaCochera.getInstancia().select(nroCochera);
				Abono abono = AdmPersistenciaAbono.getInstancia().select(nombreAbono);
												
				if (nroCheque.isEmpty() && nroTarjeta == 0 && banco.isEmpty() && fechaVto == null){
					//aca creo el contrato efectivo
					contrato = new ContratoEfectivo(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,pago,activo);
				}
				
				if (!nroCheque.isEmpty() && nroTarjeta == 0 && !banco.isEmpty() && fechaVto != null){
					//aca creo el contrato cheque
					contrato = new ContratoCheque(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,banco,nroCheque,fechaVto,pago,activo);
				}
				
				if (nroCheque.isEmpty() && nroTarjeta != 0 && !banco.isEmpty() && fechaVto != null){
					//aca creo el contrato tarjeta
					contrato = new ContratoTarjeta(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,banco,nroTarjeta,fechaVto,pago,activo);
				}
				
				
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return contrato;
		}
		catch (Exception e)	{
			System.out.println("Error en select de contrato");
		}
		return null;
	}
	
	public Contrato select(String patente){
		try	{
			Contrato contrato = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelectPatente);
			s.setString(1,patente);
			
			ResultSet result = s.executeQuery();
			
			while (result.next()){
				
				int nroContrato = result.getInt(1);
				int dni = result.getInt(2);
				Timestamp fechaDesde = result.getTimestamp(3);
				Timestamp fechaHasta = result.getTimestamp(4);				
				int nroCochera = result.getInt(6);
				String nombreAbono = result.getString(7).trim();
				String banco = (result.getObject(8) == null) ? "" : result.getString(8).trim();
				String nroCheque = (result.getObject(9) == null) ? "" : result.getString(9).trim();
				int nroTarjeta = (result.getObject(10) == null) ? 0 : result.getInt(10);
				Timestamp fechaVto = result.getTimestamp(11);
				boolean pago = Boolean.parseBoolean(result.getString(12).trim());
				boolean activo = Boolean.parseBoolean(result.getString(13).trim());
								
				Cliente cliente = AdmPersistenciaCliente.getInstancia().select(dni);
				Auto auto = AdmPersistenciaAuto.getInstancia().select(patente);
				Cochera cochera = AdmPersistenciaCochera.getInstancia().select(nroCochera);
				Abono abono = AdmPersistenciaAbono.getInstancia().select(nombreAbono);
												
				if (nroCheque.isEmpty() && nroTarjeta == 0 && banco.isEmpty() && fechaVto == null){
					//aca creo el contrato efectivo
					contrato = new ContratoEfectivo(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,pago,activo);
				}
				
				if (!nroCheque.isEmpty() && nroTarjeta == 0 && !banco.isEmpty() && fechaVto != null){
					//aca creo el contrato cheque
					contrato = new ContratoCheque(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,banco,nroCheque,fechaVto,pago,activo);
				}
				
				if (nroCheque.isEmpty() && nroTarjeta != 0 && !banco.isEmpty() && fechaVto != null){
					//aca creo el contrato tarjeta
					contrato = new ContratoTarjeta(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,banco,nroTarjeta,fechaVto,pago,activo);
				}
				
	
				
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return contrato;
		}
		catch (Exception e)	{
			System.out.println("Error en select de contrato");
		}
		return null;
	}
	
	public Vector<Contrato> selectAll(){
		try	{
			
			Vector<Contrato> contratos = new Vector<Contrato>();
			Cliente cliente = null;
			Auto auto = null;
			Cochera cochera = null;
			Abono abono = null;
			
			Connection c = PoolConnection.getPoolConnection().getConnection();
			Statement s = c.createStatement();
			ResultSet result = s.executeQuery(sentenciaSelectAll);
			
			while (result.next()){
				
				int nroContrato = result.getInt(1);
				int dni = result.getInt(2);
				Timestamp fechaDesde = result.getTimestamp(3);
				Timestamp fechaHasta = result.getTimestamp(4);
				String patente = result.getString(5).trim();
				int nroCochera = result.getInt(6);
				String nombreAbono = result.getString(7).trim();
				String banco = (result.getObject(8) == null) ? "" : result.getString(8).trim();
				String nroCheque = (result.getObject(9) == null) ? "" : result.getString(9).trim();
				int nroTarjeta = (result.getObject(10) == null) ? 0 : result.getInt(10);
				Timestamp fechaVto = result.getTimestamp(11);
				boolean pago = Boolean.parseBoolean(result.getString(12).trim());
				boolean activo = Boolean.parseBoolean(result.getString(13).trim());
				
				cliente = AdmPersistenciaCliente.getInstancia().select(dni);
				auto = AdmPersistenciaAuto.getInstancia().select(patente);
				cochera = AdmPersistenciaCochera.getInstancia().select(nroCochera);
				abono = AdmPersistenciaAbono.getInstancia().select(nombreAbono);
												
				if (nroCheque.isEmpty() && nroTarjeta == 0 && banco.isEmpty() && fechaVto == null){
					//Creo el contrato efectivo
					contratos.add(new ContratoEfectivo(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,pago,activo));
				}
				
				if (!nroCheque.isEmpty() && nroTarjeta == 0 && !banco.isEmpty() && fechaVto != null){
					//Creo el contrato cheque
					contratos.add(new ContratoCheque(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,banco,nroCheque,fechaVto,pago,activo));
				}
				
				if (nroCheque.isEmpty() && nroTarjeta != 0 && !banco.isEmpty() && fechaVto != null){
					//Creo el contrato tarjeta
					Contrato contrato = new ContratoTarjeta(nroContrato,cliente,fechaDesde,fechaHasta,auto,cochera,abono,banco,nroTarjeta,fechaVto,pago,activo);
					contratos.add(contrato);
				}
		
				
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(c);
			return contratos;
		}
		catch(Exception e)	{
			System.out.println("Error en selectAll contratos");
			
		}
		return null;
	}
	
	//INSERT
	public int insert(Object o) {
		
		try	{
			
			Contrato contrato = (Contrato)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaInsert , PreparedStatement.RETURN_GENERATED_KEYS);

			s.setInt(1, contrato.getCliente().getDNI());
			s.setTimestamp(2, contrato.getFechaDesde());
			s.setTimestamp(3, contrato.getFechaHasta());
			s.setString(4, contrato.getAuto().getPatente());
			s.setInt(5, contrato.getCochera().getNroCochera());
			s.setString(6, contrato.getAbono().getNombre());

			//Contrato Efectivo
			if ( contrato.sosEfectivo() ){
				//banco
				s.setNull(7, java.sql.Types.CHAR);
				//nroCheque
				s.setNull(8, java.sql.Types.CHAR);
				//nroTarjeta
				s.setNull(9, java.sql.Types.INTEGER );
				//fechaVto
				s.setDate(10, null);
			}
			
			//Contrato Cheque
			if ( contrato.sosCheque() ){
				//banco
				s.setString(7, ((ContratoCheque)contrato).getBanco() );
				//nroCheque
				s.setString(8, ((ContratoCheque)contrato).getNroCheque() );			
				//nroTarjeta
				s.setNull(9, java.sql.Types.INTEGER );		
				//fechaVto
				s.setTimestamp(10,((ContratoCheque)contrato).getFechaVto());

			}
			
			//Contrato Tarjeta
			if ( contrato.sosTarjeta() ){
				//banco
				s.setString(7, ((ContratoTarjeta)contrato).getBanco());
				//nroCheque
				s.setNull(8, java.sql.Types.CHAR);			
				//nroTarjeta
				s.setInt(9, ((ContratoTarjeta)contrato).getNroTarjeta() );
				//fechaVto
				s.setTimestamp(10,((ContratoTarjeta)contrato).getFechaVto() );
			}

			
			s.setString(11, String.valueOf(contrato.estaPago()) );
			s.setString(12, String.valueOf(contrato.estaActivo()) );
			
			int id = s.executeUpdate();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			
			return id;
		}
		catch (Exception e)		{
			System.out.println("Error en insert contratos");
			return -1;
		}
	}


	//UPDATE
	public void update(Object o){
		
		try
		{
			Contrato contrato = (Contrato)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaUpdate);
			
			if (contrato.getCochera() != null)
				s.setInt(1, contrato.getCochera().getNroCochera() );
			else
				s.setNull(1, java.sql.Types.INTEGER );
			
			s.setNull(2, java.sql.Types.TIMESTAMP);
			if (contrato.getFechaHasta() != null)
				s.setTimestamp(2, contrato.getFechaHasta());
						
			s.setString(3 , String.valueOf(contrato.estaPago()) );
						
			s.setInt(4, contrato.getNroContrato() );
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)		{
			System.out.println("Error en update contratos");
		}
		
	}
			
	
	//DELETE
	public void delete(Object o){
		
		try{
			
			Contrato contrato = (Contrato)o;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaDelete);
			
			s.setInt(1, contrato.getNroContrato() );
			
			s.execute();
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en delete contratos");
		}
	}

}
