package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Vector;

import model.Abono;
import model.Cliente;
import model.Recibo;


public class AdmPersistenciaRecibo{
	
	//Atributos
	private static AdmPersistenciaRecibo instancia;
	private String sentenciaInsert = "insert into SIPI.dbo.recibos values (?,?,?,?,?)";
	private String sentenciaSelectAll = "select * from SIPI.dbo.recibos where fecha BETWEEN ? AND ?";
	
	
	private AdmPersistenciaRecibo(){		
	}
	
	public static AdmPersistenciaRecibo getInstancia(){
		if (instancia == null)
			instancia = new AdmPersistenciaRecibo();
		return instancia;
	}

	//INSERT
	public void insert(Timestamp fecha, int dni, String nombreAbono, float h, float t) {
		
		try	{
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaInsert);
			
			s.setTimestamp(1, fecha );
			s.setInt(2, dni);
			s.setString(3, nombreAbono);
			s.setFloat(4, h);
			s.setFloat(5, t);

			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)		{
			System.out.println("Error en insert recibo");
		}
	}
	
	//SELECT ALL
	public Vector<Recibo> selectAll(Timestamp fechaDesde, Timestamp fechaHasta){
		try	{
			
			Vector<Recibo> recibos = new Vector<Recibo>();
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelectAll);
			s.setTimestamp(1, fechaDesde);
			s.setTimestamp(2, fechaHasta);
			
			
			ResultSet result = s.executeQuery();
			
			while (result.next()){
				
				int nroRecibo = result.getInt("nroRecibo");
				Timestamp f = result.getTimestamp("fecha");
				int  dni = result.getInt("dni");
				String nombreAbono = result.getString("nombreabono");				
				float cantHoras = result.getFloat("totalhoras"); 
				float total = result.getFloat("total");
				
				Cliente cliente = AdmPersistenciaCliente.getInstancia().selectAB(dni);
				Abono abono = AdmPersistenciaAbono.getInstancia().selectAB(nombreAbono);
				
				recibos.add( new Recibo(nroRecibo,f,cliente,abono,cantHoras,total) );				

			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return recibos;
		}
		catch (Exception e)	{
			System.out.println("Error en selectAll recibo");
			return null;
		}
		
	}

}
