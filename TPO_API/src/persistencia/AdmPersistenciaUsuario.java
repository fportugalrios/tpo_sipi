package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.*;


public class AdmPersistenciaUsuario extends AdministradorPersistencia{
	
	//Atributos
	private static AdmPersistenciaUsuario instancia;
	private String sentenciaSelect = "select * from SIPI.dbo.usuarios where alias = ?";
	private String sentenciaSelectAll = "select * from SIPI.dbo.usuarios";
	private String sentenciaInsert = "insert into SIPI.dbo.usuarios values (?,?,?,?,?)";
	private String sentenciaDelete = "delete from SIPI.dbo.usuarios where alias = ?";
	
	
	//Constructor
	private AdmPersistenciaUsuario(){		
	}
	
	public static AdmPersistenciaUsuario getInstancia(){
		if (instancia == null)
			instancia = new AdmPersistenciaUsuario();
		return instancia;
	}


	//SELECT
	public Usuario select(String alias){
		try	{
			
			Usuario usuario = null;
			
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaSelect);
			s.setString(1,alias);
			
			ResultSet result = s.executeQuery();
			
			while (result.next()){
				
				String a = result.getString(1).trim();
				String pass = result.getString(2).trim();
				String nombreReal = result.getString(3).trim();
				String permiso = result.getString(4).trim();
				boolean activo = Boolean.parseBoolean( result.getString(5).trim() );
				
				usuario = new Usuario(a,pass,nombreReal, Permiso.valueOf(permiso) ,activo);			
				
			}
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
			return usuario;
		}
		catch (Exception e)		{
			System.out.println("Error en select usuarios");
			return null;
		}
		
	}
	
	//SELECT  ALL
	public List<Usuario> selectAll(){
		try	{
			
			List<Usuario> usuarios = new ArrayList<Usuario>();
			
			Connection c = PoolConnection.getPoolConnection().getConnection();
			Statement s = c.createStatement();
			ResultSet result = s.executeQuery(sentenciaSelectAll);
			
			while (result.next()){
				
				String a = result.getString(1).trim();
				String pass = result.getString(2).trim();
				String nombreReal = result.getString(3).trim();
				String permiso = result.getString(4).trim();
				boolean activo = Boolean.parseBoolean( result.getString(5).trim() );

				Usuario usuario = new Usuario(a,pass,nombreReal, Permiso.valueOf(permiso) ,activo);
				
				usuarios.add(usuario);
				
			}
			PoolConnection.getPoolConnection().realeaseConnection(c);
			return usuarios;
		}
		catch(Exception e)	{
			System.out.println("Error en selectAll usuarios");
			return null;
			
		}
		
	}
	

	
	//INSERT
	public void insert(Object o) {
		
		try	{
			
			Usuario usuario= (Usuario)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			
			PreparedStatement s = con.prepareStatement(sentenciaInsert);

			s.setString(1, usuario.getAlias());
			s.setString(2, usuario.getPassword());
			s.setString(3, usuario.getNombreReal());
			s.setString(4, usuario.getPermiso().toString() );
			s.setString(5, "true");
			
			s.execute();
			
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en insert usuarios");
		}
	}


	//UPDATE
	public void update(Object o){
		
	}
			
	
	//DELETE
	public void delete(Object o){
		
		try{
			Usuario usuario = (Usuario)o;
			Connection con = PoolConnection.getPoolConnection().getConnection();
			PreparedStatement s = con.prepareStatement(sentenciaDelete);
			s.setString(1, usuario.getAlias() );
			s.execute();
			PoolConnection.getPoolConnection().realeaseConnection(con);
		}
		catch (Exception e)	{
			System.out.println("Error en delete usuarios");
		}
	}


}
