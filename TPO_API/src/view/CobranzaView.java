package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import controlador.Sistema;

import java.awt.event.ActionListener;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.List;
import java.awt.event.ActionEvent;

import javax.swing.JSeparator;

import model.ContratoView;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;

public class CobranzaView extends JFrame {

	private static final long serialVersionUID = 4955625203973577699L;
	private JPanel contentPane;
	private JTextField total;
	private JTextField dni;
	private JTextField nombreCliente;
	private JTextField nombreAbono;
	private JTextField precio;
	private JTextField fechaDesde;
	private JTextField totalHoras;
	private JTextField fechaHasta;
	private JTextField horas;
	private ContratoView contrato = null;
	private JComboBox contratos;
	private List<ContratoView> contratosView;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CobranzaView frame = new CobranzaView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CobranzaView() {
		setTitle("Cobrar Cliente");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 583, 504);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		
		JLabel lblDni = new JLabel("Dni");
		lblDni.setBounds(20, 97, 44, 21);
		contentPane.add(lblDni);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(204, 97, 59, 21);
		contentPane.add(lblNombre);
		
		JButton bCrear = new JButton("CERRAR");
		bCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
					close();				
			}
		});
		bCrear.setBounds(297, 436, 105, 23);
		contentPane.add(bCrear);
		
		JLabel lblContrato = new JLabel("Contrato");
		lblContrato.setBounds(20, 61, 66, 21);
		contentPane.add(lblContrato);
		
		JLabel lblnombreAbono = new JLabel("Abono");
		lblnombreAbono.setBounds(20, 193, 53, 21);
		contentPane.add(lblnombreAbono);
		
		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(393, 193, 53, 21);
		contentPane.add(lblPrecio);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(8, 129, 559, 2);
		contentPane.add(separator);
		
		JLabel lblFechaIngreso = new JLabel("Entrada");
		lblFechaIngreso.setBounds(20, 294, 53, 21);
		contentPane.add(lblFechaIngreso);
		
		JLabel lblFechaEgreso = new JLabel("Salida");
		lblFechaEgreso.setBounds(325, 294, 53, 21);
		contentPane.add(lblFechaEgreso);
		
		JLabel lblcantHoras = new JLabel("Horas");
		lblcantHoras.setBounds(20, 326, 44, 21);
		contentPane.add(lblcantHoras);
		
		JLabel lblTotal = new JLabel("TOTAL");
		lblTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTotal.setBounds(380, 387, 53, 21);
		contentPane.add(lblTotal);
		
		dni = new JTextField();
		dni.setEditable(false);
		dni.setBounds(77, 97, 97, 20);
		contentPane.add(dni);
		dni.setColumns(10);
		
		nombreCliente = new JTextField();
		nombreCliente.setEditable(false);
		nombreCliente.setColumns(10);
		nombreCliente.setBounds(263, 97, 135, 20);
		contentPane.add(nombreCliente);
		
		nombreAbono = new JTextField();
		nombreAbono.setEditable(false);
		nombreAbono.setColumns(10);
		nombreAbono.setBounds(69, 193, 105, 20);
		contentPane.add(nombreAbono);
		
		precio = new JTextField();
		precio.setEditable(false);
		precio.setColumns(10);
		precio.setBounds(446, 193, 105, 20);
		contentPane.add(precio);
		
		fechaDesde = new JTextField();
		fechaDesde.setEditable(false);
		fechaDesde.setColumns(10);
		fechaDesde.setBounds(73, 294, 163, 21);
		contentPane.add(fechaDesde);
		
		totalHoras = new JTextField();
		totalHoras.setEditable(false);
		totalHoras.setColumns(10);
		totalHoras.setBounds(73, 326, 163, 20);
		contentPane.add(totalHoras);
		
		fechaHasta = new JTextField();
		fechaHasta.setEditable(false);
		fechaHasta.setColumns(10);
		fechaHasta.setBounds(370, 294, 163, 20);
		contentPane.add(fechaHasta);
		
		total = new JTextField();
		total.setEditable(false);
		total.setBounds(437, 387, 108, 20);
		contentPane.add(total);
		total.setColumns(10);
		
		JLabel lblHoras = new JLabel("Horas");
		lblHoras.setBounds(205, 193, 53, 21);
		contentPane.add(lblHoras);
		
		horas = new JTextField();
		horas.setText("0.0");
		horas.setEditable(false);
		horas.setColumns(10);
		horas.setBounds(258, 193, 105, 20);
		contentPane.add(horas);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(8, 21, 559, 2);
		contentPane.add(separator_3);		
		
		JLabel lblDatosDelCliente = new JLabel("CLIENTE");
		lblDatosDelCliente.setBounds(20, 27, 117, 21);
		contentPane.add(lblDatosDelCliente);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBounds(9, 49, 557, 2);
		contentPane.add(separator_4);
		
		JLabel lblTipoDeAbono = new JLabel("ABONO");
		lblTipoDeAbono.setBounds(20, 157, 117, 21);
		contentPane.add(lblTipoDeAbono);
		
		JSeparator separator_6 = new JSeparator();
		separator_6.setOrientation(SwingConstants.VERTICAL);
		separator_6.setBounds(8, 21, 13, 109);
		contentPane.add(separator_6);
		
		JSeparator separator_7 = new JSeparator();
		separator_7.setOrientation(SwingConstants.VERTICAL);
		separator_7.setBounds(567, 21, 5, 109);
		contentPane.add(separator_7);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 180, 557, 2);
		contentPane.add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(9, 153, 559, 2);
		contentPane.add(separator_2);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setBounds(9, 225, 559, 2);
		contentPane.add(separator_5);
		
		JSeparator separator_8 = new JSeparator();
		separator_8.setOrientation(SwingConstants.VERTICAL);
		separator_8.setBounds(568, 153, 6, 73);
		contentPane.add(separator_8);
		
		JSeparator separator_9 = new JSeparator();
		separator_9.setOrientation(SwingConstants.VERTICAL);
		separator_9.setBounds(8, 153, 13, 73);
		contentPane.add(separator_9);
		
		JSeparator separator_10 = new JSeparator();
		separator_10.setOrientation(SwingConstants.VERTICAL);
		separator_10.setBounds(7, 253, 13, 110);
		contentPane.add(separator_10);
		
		JSeparator separator_11 = new JSeparator();
		separator_11.setOrientation(SwingConstants.VERTICAL);
		separator_11.setBounds(567, 252, 8, 112);
		contentPane.add(separator_11);
		
		JSeparator separator_12 = new JSeparator();
		separator_12.setBounds(7, 252, 560, 2);
		contentPane.add(separator_12);
		
		JSeparator separator_13 = new JSeparator();
		separator_13.setBounds(10, 280, 557, 2);
		contentPane.add(separator_13);
		
		JLabel lblPeriodo = new JLabel("PERIODO");
		lblPeriodo.setBounds(20, 257, 117, 21);
		contentPane.add(lblPeriodo);
		
		JSeparator separator_14 = new JSeparator();
		separator_14.setBounds(7, 363, 560, 2);
		contentPane.add(separator_14);
		
		JSeparator separator_15 = new JSeparator();
		separator_15.setBounds(370, 371, 195, 3);
		contentPane.add(separator_15);
		
		JSeparator separator_16 = new JSeparator();
		separator_16.setBounds(370, 418, 195, 3);
		contentPane.add(separator_16);
		
		JSeparator separator_17 = new JSeparator();
		separator_17.setOrientation(SwingConstants.VERTICAL);
		separator_17.setBounds(565, 371, 8, 48);
		contentPane.add(separator_17);
		
		JSeparator separator_18 = new JSeparator();
		separator_18.setOrientation(SwingConstants.VERTICAL);
		separator_18.setBounds(369, 371, 8, 48);
		contentPane.add(separator_18);		
		
		JButton btnBuscar = new JButton("Cobrar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if ( !contratos.getSelectedItem().equals("Seleccione") ){
					
					int nro = Integer.valueOf( contratos.getSelectedItem().toString() );
					
					for (ContratoView c : contratosView)
						if ( c.getNroContrato() == nro){
							contrato = c;
							break;
						}
					
					if (contrato != null ){
						
						DecimalFormat df = new DecimalFormat("#.##");
						df.setRoundingMode(RoundingMode.CEILING);
						java.util.Date date= new java.util.Date();
						Timestamp fecha = new Timestamp(date.getTime());
						
						dni.setText( Integer.toString(contrato.getCliente().getDNI()) );
						nombreCliente.setText( contrato.getCliente().getNombre()  );
						nombreAbono.setText( contrato.getAbono().getNombre()  );
						precio.setText( Float.toString(contrato.getAbono().getPrecio()) );
						horas.setText(  Integer.toString(contrato.getAbono().getHoras())  );
						fechaDesde.setText( contrato.getFechaDesde().toString().substring(0, 19) );		
						fechaHasta.setText( fecha.toString().toString().substring(0, 19) );
						
						
						float h = Sistema.getInstancia().calcularHoras(contrato.getFechaDesde(), fecha );
						totalHoras.setText( df.format(h) );
						
						float t = Sistema.getInstancia().cobrarContrato(contrato.getNroContrato());
						total.setText( df.format(t) );
						
					}
					
					else
						JOptionPane.showMessageDialog(null, "Contrato no encontrado");
					
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un contrato");
					
				
				
				
				
			}
		});
		btnBuscar.setBounds(440, 78, 95, 23);
		contentPane.add(btnBuscar);
		
		JButton btnImprimir = new JButton("IMPRIMIR");
		btnImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ( !dni.getText().isEmpty() ){
					JOptionPane.showMessageDialog(null, "Imprimiendo");
					close();
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un contrato");
			}
		});
		btnImprimir.setBounds(168, 436, 105, 23);
		contentPane.add(btnImprimir);
		
		contratos = new JComboBox();
		
		contratos.setBounds(77, 61, 97, 20);
		contentPane.add(contratos);
		contratos.addItem("Seleccione");
		
		JSeparator separator_19 = new JSeparator();
		separator_19.setOrientation(SwingConstants.VERTICAL);
		separator_19.setBounds(411, 49, 5, 81);
		contentPane.add(separator_19);
		
		contratosView = Sistema.getInstancia().buscarContratosView();
		
		for (ContratoView c : contratosView)
			contratos.addItem( c.getNroContrato() );
		
	}
	
	public void close(){
		this.dispose();
	}
}
