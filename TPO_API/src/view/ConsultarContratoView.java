package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.Sistema;
import model.ContratoView;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;


public class ConsultarContratoView extends JFrame {


	private static final long serialVersionUID = 5883975647218529513L;
	private JPanel contentPane;
	private JTextField nombreCliente;
	private JTextField nroCochera;
	private JTextField tipo;
	private JTextField fechaVto;
	private JTextField nroTarjeta;
	private JLabel lBanco;
	private JLabel lCBU;
	private JLabel lVencimiento;
	private JTextField marca;
	private JTextField banco;
	private JTextField nroCheque;
	private JLabel lblNewLabel;
	private JTextField nombreAbono;
	private JTextField DNI;
	private JTextField precio;
	private JComboBox comboPatentes;
	List<ContratoView> contratos;
	private JTextField nroContrato;
	private JTextField modelo;
	private JTextField pago;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultarContratoView frame = new ConsultarContratoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultarContratoView() {
		setTitle("Consultar Contrato");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 592);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("CLIENTE");
		lblDni.setBounds(14, 67, 91, 21);
		contentPane.add(lblDni);
		
		nombreCliente = new JTextField();
		nombreCliente.setEditable(false);
		nombreCliente.setColumns(10);
		nombreCliente.setBounds(111, 99, 171, 20);
		contentPane.add(nombreCliente);
		
		JLabel lblCochera = new JLabel("Cochera");
		lblCochera.setBounds(35, 182, 66, 21);
		contentPane.add(lblCochera);
		
		nroCochera = new JTextField();
		nroCochera.setEditable(false);
		nroCochera.setColumns(10);
		nroCochera.setBounds(111, 180, 171, 20);
		contentPane.add(nroCochera);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(376, 182, 66, 21);
		contentPane.add(lblTipo);
		
		tipo = new JTextField();
		tipo.setEditable(false);
		tipo.setColumns(10);
		tipo.setBounds(449, 182, 171, 20);
		contentPane.add(tipo);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(35, 213, 66, 21);
		contentPane.add(lblMarca);
		
		JButton bCrear = new JButton("CERRAR");
		bCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			close();
				
			}
		});
		bCrear.setBounds(273, 516, 105, 23);
		contentPane.add(bCrear);
		
		JLabel Nombre = new JLabel("Nombre");
		Nombre.setBounds(35, 294, 103, 21);
		contentPane.add(Nombre);
		
		lBanco = new JLabel("Banco");
		lBanco.setBounds(35, 378, 91, 21);
		contentPane.add(lBanco);
		
		lVencimiento = new JLabel("Vencimiento");
		lVencimiento.setBounds(35, 452, 91, 21);
		contentPane.add(lVencimiento);
		
		fechaVto = new JTextField();
		fechaVto.setEditable(false);
		fechaVto.setColumns(10);
		fechaVto.setBounds(107, 452, 175, 20);
		contentPane.add(fechaVto);
		fechaVto.setText("");
		
		lCBU = new JLabel("Tarjeta");
		lCBU.setBounds(35, 412, 91, 21);
		contentPane.add(lCBU);
		
		nroTarjeta = new JTextField();
		nroTarjeta.setEditable(false);
		nroTarjeta.setColumns(10);
		nroTarjeta.setBounds(107, 414, 175, 20);
		contentPane.add(nroTarjeta);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(14, 261, 631, 2);
		contentPane.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(11, 341, 631, 2);
		contentPane.add(separator_1);
		
		marca = new JTextField();
		marca.setEditable(false);
		marca.setColumns(10);
		marca.setBounds(111, 214, 171, 20);
		contentPane.add(marca);
		
		banco = new JTextField();
		banco.setEditable(false);
		banco.setColumns(10);
		banco.setBounds(107, 378, 175, 20);
		contentPane.add(banco);
		
		nroCheque = new JTextField();
		nroCheque.setEditable(false);
		nroCheque.setColumns(10);
		nroCheque.setBounds(449, 413, 171, 20);
		contentPane.add(nroCheque);
		
		lblNewLabel = new JLabel("Cheque");
		lblNewLabel.setBounds(376, 416, 46, 14);
		contentPane.add(lblNewLabel);
		
		nombreAbono = new JTextField();
		nombreAbono.setEditable(false);
		nombreAbono.setColumns(10);
		nombreAbono.setBounds(111, 294, 171, 20);
		contentPane.add(nombreAbono);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(10, 67, 631, 2);
		contentPane.add(separator_2);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(10, 147, 631, 2);
		contentPane.add(separator_3);
		
		comboPatentes = new JComboBox();		
		comboPatentes.setBounds(111, 25, 171, 23);
		contentPane.add(comboPatentes);
		comboPatentes.addItem("Seleccione");	
		
		contratos = Sistema.getInstancia().buscarContratosView();
		
		if (contratos != null)
			for (ContratoView c : contratos)
				comboPatentes.addItem( c.getAuto().getPatente() );
		
		comboPatentes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ( !comboPatentes.getSelectedItem().equals("Seleccione") ){
					
					for (ContratoView c : contratos)
						if ( c.getAuto().getPatente().equals(comboPatentes.getSelectedItem().toString() ) ){
							
							nroContrato.setText( String.valueOf(c.getNroContrato()) );
							nombreCliente.setText(c.getCliente().getNombre());
							DNI.setText( String.valueOf(c.getCliente().getDNI()) );
							String nro = ( c.getCochera() != null ) ? String.valueOf(c.getCochera().getNroCochera()) : ""; 
							String piso = ( c.getCochera() != null ) ? " (P" + String.valueOf(c.getCochera().getPiso()) + ")" : "";
							nroCochera.setText( nro + piso  );
							tipo.setText( c.getAuto().getTipo() );
							marca.setText( c.getAuto().getMarca() );
							modelo.setText( c.getAuto().getModelo() );
							nombreAbono.setText(c.getAbono().getNombre());
							precio.setText( String.valueOf(c.getAbono().getPrecio()) );					
							banco.setText(  c.getBanco().equals("") ? "" : c.getBanco()  );
							nroCheque.setText( c.getNroCheque().equals("") ? "" : c.getNroCheque() );
							nroTarjeta.setText( (c.getNroTarjeta() == 0) ? "" : String.valueOf(c.getNroTarjeta()) );
							fechaVto.setText( (c.getFechaVto() == null) ? "" : c.getFechaVto().toString().substring(0, 7)  );	
							if ( c.estaPago() ) 
								pago.setText("SI");
							else
								pago.setText("NO");
							
							break;
						}
								
				}
			}
		});
		
		JLabel lblContrato = new JLabel("Patente");
		lblContrato.setBounds(35, 29, 70, 14);
		contentPane.add(lblContrato);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBounds(10, 86, 631, 2);
		contentPane.add(separator_4);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(35, 99, 66, 21);
		contentPane.add(lblNombre);
		
		JLabel lblDni_1 = new JLabel("DNI");
		lblDni_1.setBounds(376, 99, 48, 21);
		contentPane.add(lblDni_1);
		
		DNI = new JTextField();
		DNI.setEditable(false);
		DNI.setColumns(10);
		DNI.setBounds(449, 99, 171, 20);
		contentPane.add(DNI);
		
		JLabel lblVehculo_1 = new JLabel("VEHICULO");
		lblVehculo_1.setBounds(14, 148, 91, 21);
		contentPane.add(lblVehculo_1);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setBounds(10, 167, 631, 2);
		contentPane.add(separator_5);
		
		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(376, 294, 103, 21);
		contentPane.add(lblPrecio);
		
		precio = new JTextField();
		precio.setEditable(false);
		precio.setColumns(10);
		precio.setBounds(449, 294, 171, 20);
		contentPane.add(precio);
		
		JLabel lblAbono = new JLabel("ABONO");
		lblAbono.setBounds(18, 262, 91, 21);
		contentPane.add(lblAbono);
		
		JSeparator separator_6 = new JSeparator();
		separator_6.setBounds(14, 281, 631, 2);
		contentPane.add(separator_6);
		
		JLabel lblPago = new JLabel("FORMA DE PAGO");
		lblPago.setBounds(18, 341, 91, 21);
		contentPane.add(lblPago);
		
		JSeparator separator_7 = new JSeparator();
		separator_7.setBounds(13, 360, 631, 2);
		contentPane.add(separator_7);
		
		JLabel lblContrato_1 = new JLabel("Contrato");
		lblContrato_1.setBounds(373, 25, 66, 21);
		contentPane.add(lblContrato_1);
		
		nroContrato = new JTextField();
		nroContrato.setEditable(false);
		nroContrato.setColumns(10);
		nroContrato.setBounds(449, 25, 171, 20);
		contentPane.add(nroContrato);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(376, 214, 66, 21);
		contentPane.add(lblModelo);
		
		modelo = new JTextField();
		modelo.setEditable(false);
		modelo.setColumns(10);
		modelo.setBounds(449, 214, 171, 20);
		contentPane.add(modelo);
		
		JLabel lblPago_1 = new JLabel("Pago");
		lblPago_1.setBounds(376, 378, 66, 21);
		contentPane.add(lblPago_1);
		
		pago = new JTextField();
		pago.setEditable(false);
		pago.setColumns(10);
		pago.setBounds(449, 378, 46, 20);
		contentPane.add(pago);
	

			
	}
	
	public void close(){
		this.dispose();		
	}
}
