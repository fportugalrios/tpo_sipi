package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import controlador.Sistema;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class CrearAbonoView extends JFrame {


	private static final long serialVersionUID = -7970142016805039478L;
	private JPanel contentPane;
	private JTextField nombre;
	private JTextField precio;
	private JTextField horas;
	private JTextField descuento;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearAbonoView frame = new CrearAbonoView(); 
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CrearAbonoView() {
		setTitle("Creacion de Abono");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("Nombre");
		lblDni.setBounds(35, 39, 66, 21);
		contentPane.add(lblDni);
		
		nombre = new JTextField();
		nombre.setColumns(10);
		nombre.setBounds(104, 40, 194, 20);
		contentPane.add(nombre);
		
		JLabel lblNombre = new JLabel("Horas");
		lblNombre.setBounds(35, 91, 66, 21);
		contentPane.add(lblNombre);
		
		JLabel lblTelefono = new JLabel("Precio");
		lblTelefono.setBounds(35, 140, 66, 21);
		contentPane.add(lblTelefono);
		
		precio = new JTextField();
		precio.setColumns(10);
		precio.setBounds(104, 141, 94, 20);
		contentPane.add(precio);
		
		JButton bCrear = new JButton("CREAR");
		bCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (!vacio() && Sistema.getInstancia().esNumerico(horas.getText()) && Sistema.getInstancia().esNumerico(precio.getText()) && 
						Sistema.getInstancia().esNumerico(descuento.getText()) ){
					
					if ( validarCampos() ){
						
						int	h;
						float p;
						float d;
						boolean exito = false;
						boolean fueraRango = false;
										
						try {
							h = Integer.parseInt(horas.getText());
							p = Float.parseFloat(precio.getText());
							d = Float.parseFloat(descuento.getText());
							exito = Sistema.getInstancia().crearAbono(nombre.getText(),h,p,d);
						} catch (NumberFormatException e1) {
							fueraRango = true;
						}
						
						if (exito)
							close();						
						else
							if ( !fueraRango)
								JOptionPane.showMessageDialog(null, "Abono ya existe");
							else
								JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inv�lidos");
						
					}
					else
						JOptionPane.showMessageDialog(null, "Excedi� el limite de caracteres de alguno de los campos");
						
									
						
					
				}
				else
					JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inv�lidos");
				
			}	
		});
		bCrear.setBounds(208, 273, 105, 23);
		contentPane.add(bCrear);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(370, 273, 105, 23);
		contentPane.add(bCancelar);
		
		horas = new JTextField();
		horas.setColumns(10);
		horas.setBounds(104, 91, 94, 20);
		contentPane.add(horas);
		
		JLabel lblDias = new JLabel("Descuento");
		lblDias.setBounds(35, 190, 66, 21);
		contentPane.add(lblDias);
		
		descuento = new JTextField();
		descuento.setColumns(10);
		descuento.setBounds(104, 190, 94, 20);
		contentPane.add(descuento);
	}
	
	public void close(){
		this.dispose();		
	}
	
	private boolean validarCampos(){
		if ( nombre.getText().length() <= 30 )
			return true;
		else
			return false;
	}
	
	public boolean vacio(){
		if (nombre.getText().isEmpty() || horas.getText().isEmpty() || precio.getText().isEmpty() || descuento.getText().isEmpty())
			return true;
		else
			return false;
	}
}
