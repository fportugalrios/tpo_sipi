package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import controlador.Sistema;

public class CrearBancoView extends JFrame {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9112594400226290608L;
	private JPanel contentPane;
	private JTextField banco;
	private JTextField url;
	private JTextField archivo;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearBancoView frame = new CrearBancoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CrearBancoView() {
		setTitle("Creacion de Banco");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblModelo = new JLabel("Banco");
		lblModelo.setBounds(35, 44, 66, 21);
		contentPane.add(lblModelo);
		
		banco = new JTextField();
		banco.setColumns(10);
		banco.setBounds(92, 46, 194, 20);
		contentPane.add(banco);
		
		JLabel lblModelo_1 = new JLabel("URL");
		lblModelo_1.setBounds(35, 89, 66, 21);
		contentPane.add(lblModelo_1);
		
		url = new JTextField();
		url.setColumns(10);
		url.setBounds(92, 91, 194, 20);
		contentPane.add(url);
		
		JButton bCrear = new JButton("CREAR");
		bCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (!vacio()){					
					
					if ( validarCampos() ){
						
						boolean exito = Sistema.getInstancia().crearBanco(banco.getText(), url.getText(), archivo.getText());
						
						if (exito)
							close();										
						else
							JOptionPane.showMessageDialog(null, "Banco ya existe");
						
					}
					else
						JOptionPane.showMessageDialog(null, "Excedi� el limite de caracteres de alguno de los campos");
				}
				else
					JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inv�lidos");
				
			}
		});
		bCrear.setBounds(214, 267, 105, 23);
		contentPane.add(bCrear);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(376, 267, 105, 23);
		contentPane.add(bCancelar);
		
		JLabel lblCheque = new JLabel("Archivo");
		lblCheque.setBounds(35, 139, 91, 21);
		contentPane.add(lblCheque);
		
		archivo = new JTextField();
		archivo.setColumns(10);
		archivo.setBounds(92, 141, 194, 20);
		contentPane.add(archivo);
	}
	
	public void close(){
		this.dispose();
	}
	
	private boolean validarCampos(){
		
		if (banco.getText().length() <= 30 && url.getText().length() <= 100 && archivo.getText().length() <= 100)
			return true;
		else
			return false;
	}
	
	public boolean vacio(){
		if (banco.getText().isEmpty() || url.getText().isEmpty() || archivo.getText().isEmpty())
			return true;
		else
			return false;
	}
}
