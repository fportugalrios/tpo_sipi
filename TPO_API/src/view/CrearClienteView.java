package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import controlador.Sistema;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CrearClienteView extends JFrame {

	private static final long serialVersionUID = -4956625203973577699L;
	private JPanel contentPane;
	private JTextField DNI;
	private JTextField nombre;
	private JTextField domicilio;
	private JTextField telefono;
	private JTextField mail;
	private static CrearClienteView instancia;
	
	public static CrearClienteView getInstancia(){
		if (instancia == null)
			instancia = new CrearClienteView();
		return instancia;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearClienteView frame = new CrearClienteView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CrearClienteView() {
		setTitle("Creacion de Cliente");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("DNI");
		lblDni.setBounds(35, 39, 66, 21);
		contentPane.add(lblDni);
		
		DNI = new JTextField();
		DNI.setColumns(10);
		DNI.setBounds(96, 40, 194, 20);
		contentPane.add(DNI);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(35, 83, 66, 21);
		contentPane.add(lblNombre);
		
		nombre = new JTextField();
		nombre.setColumns(10);
		nombre.setBounds(96, 84, 194, 20);
		contentPane.add(nombre);
		
		domicilio = new JTextField();
		domicilio.setColumns(10);
		domicilio.setBounds(96, 128, 194, 20);
		contentPane.add(domicilio);
		
		JLabel lblDomicilio = new JLabel("Domicilio");
		lblDomicilio.setBounds(35, 127, 66, 21);
		contentPane.add(lblDomicilio);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(35, 175, 66, 21);
		contentPane.add(lblTelefono);
		
		telefono = new JTextField();
		telefono.setColumns(10);
		telefono.setBounds(96, 176, 194, 20);
		contentPane.add(telefono);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(35, 222, 66, 21);
		contentPane.add(lblMail);
		
		mail = new JTextField();
		mail.setColumns(10);
		mail.setBounds(96, 223, 194, 20);
		contentPane.add(mail);
		
		JButton bCrear = new JButton("CREAR");
		bCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !vacio() && Sistema.getInstancia().esNumerico(DNI.getText()) 
						&& Sistema.getInstancia().esNumerico(telefono.getText()) && Sistema.getInstancia().validarMail(mail.getText()) ){
						
					if ( validarCampos() ){
						
						int dni;
						int tel; 
						boolean exito = false;
						boolean fueraRango = false;
						
						try {
							dni = Integer.parseInt(DNI.getText());;
							tel = Integer.parseInt(telefono.getText());						
							exito = Sistema.getInstancia().crearCliente(dni,nombre.getText(),domicilio.getText(),tel,mail.getText());
						} catch (NumberFormatException e1) {
							fueraRango = true;
						}
						
						if (exito){
							limpiarPantalla();									
							close();						
						}
						else
							if ( !fueraRango )
								JOptionPane.showMessageDialog(null, "Cliente ya existe");
							else
								JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inv�lidos");
					}
					else
						JOptionPane.showMessageDialog(null, "Excedi� el limite de caracteres de alguno de los campos");
				}
				else
					JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inv�lidos");
			}
		});
		bCrear.setBounds(203, 295, 105, 23);
		contentPane.add(bCrear);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarPantalla();
				close();
			}
		});
		bCancelar.setBounds(365, 295, 105, 23);
		contentPane.add(bCancelar);
	}
	
	public void limpiarPantalla(){
		DNI.setText("");
		nombre.setText("");
		domicilio.setText("");
		telefono.setText("");
		mail.setText("");
	}
	
	public boolean vacio(){
		if (DNI.getText().isEmpty() || nombre.getText().isEmpty() || domicilio.getText().isEmpty() || 
				telefono.getText().isEmpty() || mail.getText().isEmpty())
			return true;
		else
			return false;
	}
	
	private boolean validarCampos(){
		
		if (nombre.getText().length() <= 30 && domicilio.getText().length() <= 50 && mail.getText().length() <= 50)
			return true;
		else
			return false;
	}
	
	public void close(){
		this.dispose();
	}
}
