package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;

import controlador.Sistema;
import model.ContratoView;

import javax.swing.JSeparator;
import model.FormaPago;

public class CrearContratoView extends JFrame {


	private static final long serialVersionUID = 5883975647218529513L;
	private JPanel contentPane;
	private JTextField DNI;
	private JTextField patente;
	private JTextField modelo;
	private JTextField nroCheque;
	private JTextField nroTarjeta;
	private JTextField fechaVto;
	private JTextField CBU;
	private JComboBox comboTipo;
	private JComboBox comboPago;
	private JLabel lBanco;
	private JLabel lCheque;
	private JLabel lTarjeta;
	private JLabel lCBU;
	private JLabel lVencimiento;
	private JComboBox comboAbono;
	private JComboBox banco;
	private JComboBox comboMarcas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearContratoView frame = new CrearContratoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CrearContratoView() {
		setTitle("Creacion de Contrato");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 539);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("DNI");
		lblDni.setBounds(35, 25, 91, 21);
		contentPane.add(lblDni);
		
		DNI = new JTextField();
		DNI.setColumns(10);
		DNI.setBounds(98, 27, 194, 20);
		contentPane.add(DNI);
		
		JLabel lblPatente = new JLabel("Patente");
		lblPatente.setBounds(35, 71, 66, 21);
		contentPane.add(lblPatente);
		
		patente = new JTextField();
		patente.setColumns(10);
		patente.setBounds(98, 73, 194, 20);
		contentPane.add(patente);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(35, 121, 66, 21);
		contentPane.add(lblMarca);
		
		JLabel lblModelo_1 = new JLabel("Modelo");
		lblModelo_1.setBounds(358, 121, 66, 21);
		contentPane.add(lblModelo_1);
		
		modelo = new JTextField();
		modelo.setColumns(10);
		modelo.setBounds(425, 121, 171, 20);
		contentPane.add(modelo);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(358, 71, 66, 21);
		contentPane.add(lblTipo);
		
		JLabel lblPago = new JLabel("Pago");
		lblPago.setBounds(35, 261, 103, 21);
		contentPane.add(lblPago);
		
		comboTipo = new JComboBox();
		comboTipo.setModel(new DefaultComboBoxModel(new String[] {"Auto", "Camioneta"}));
		comboTipo.setBounds(428, 71, 112, 20);
		contentPane.add(comboTipo);
		
		JButton bCrear = new JButton("CREAR");
		bCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (!vacio() && Sistema.getInstancia().esNumerico(DNI.getText()) &&
						Sistema.getInstancia().esNumerico(nroTarjeta.getText()) && 
						Sistema.getInstancia().esNumerico(CBU.getText())){
					
					int dni = 0;
					int tarjeta = 0;
					int cbu = 0;	
					boolean fueraRango = false;
					
					try {
						dni = Integer.parseInt(DNI.getText());
						tarjeta = Integer.parseInt(nroTarjeta.getText());
						cbu = Integer.parseInt(CBU.getText());			
					} catch (NumberFormatException e1) {
						fueraRango = true;
					}
					
					if (!fueraRango){
						
						if ( Sistema.getInstancia().existeCliente(dni) ){
							
							if ( validarCampos() ){
								
								String abono = comboAbono.getSelectedItem().toString();
								String tipo = comboTipo.getSelectedItem().toString();
								String pago = comboPago.getSelectedItem().toString();
								String b = banco.getSelectedItem().toString();
								String fVto = "";							
								boolean contratoCreado;						
								boolean fechaValida = true;
								
								if ( pago.equals("Tarjeta") || pago.equals("Cheque") ){
							
									fVto = fechaVto.getText() + "-30 00:00:00";
									
									if ( Sistema.getInstancia().validarFecha( fVto ) )
										if ( Sistema.getInstancia().validarFechaVto(fVto) )
											fechaValida = true;
										else
											fechaValida = false;							
									else
										fechaValida = false;

								}
								
								if (fechaValida){
									
									java.util.Date date= new java.util.Date();
									Timestamp fecha = new Timestamp(date.getTime());

									contratoCreado = Sistema.getInstancia().crearContrato(dni,fecha.toString(),patente.getText(), 
											comboMarcas.getSelectedItem().toString(), modelo.getText(),tipo, pago, b , 
											nroCheque.getText(), tarjeta, fVto ,cbu,abono);
									
									if (contratoCreado){
										//Cierro las ventanas que vengo abriendo
										ContratoView contrato = Sistema.getInstancia().buscarContratoPatenteView(patente.getText());
										
										String extension = "";
										String piso = " (P" + String.valueOf( contrato.getCochera().getPiso() ) + ")";
										if ( contrato.getCochera().getExtension() != -1)
											extension = " y " + String.valueOf( contrato.getCochera().getExtension() );
										
										String msg = "CONTRATO " + contrato.getNroContrato() +  " - COCHERA " + contrato.getCochera().getNroCochera() + extension + piso;
										JOptionPane.showMessageDialog(null, msg);
										limpiarPantalla();
										close();
									}
									else
										if ( Sistema.getInstancia().existePatente(patente.getText()) )
											JOptionPane.showMessageDialog(null, "Patente ya existe");
										else
											JOptionPane.showMessageDialog(null, "No hay cocheras disponibles");
									
								}
								else
									JOptionPane.showMessageDialog(null, "Fecha Inv�lida");
								
							}
							else
								JOptionPane.showMessageDialog(null, "Excedi� el limite de caracteres de alguno de los campos");
						}
						else{
							CrearClienteView vCrearCliente = new CrearClienteView();
							vCrearCliente.setVisible(true);
						}
						
					}
					else
						JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inv�lidos");
					
				}
				else
					JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inv�lidos");
				
			}
		});
		bCrear.setBounds(218, 447, 105, 23);
		contentPane.add(bCrear);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(380, 447, 105, 23);
		contentPane.add(bCancelar);
		
		comboPago = new JComboBox();		
		comboPago.setModel(new DefaultComboBoxModel(FormaPago.values()));
		comboPago.setBounds(98, 262, 165, 20);
		contentPane.add(comboPago);
		comboPago.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mostrarCampos();
			}
		});
		
		JLabel lblAbono = new JLabel("Abono");
		lblAbono.setBounds(35, 190, 103, 21);
		contentPane.add(lblAbono);

		//Busco los abonos disponibles para vender y los muestros en el comboAbono		
		Vector<String> nombres = Sistema.getInstancia().buscarDisponibilidad();

		comboAbono = new JComboBox();
		comboAbono.setModel(new DefaultComboBoxModel(nombres));
		comboAbono.setBounds(98, 191, 165, 20);
		contentPane.add(comboAbono);

		
		lBanco = new JLabel("Banco");
		lBanco.setBounds(35, 313, 91, 21);
		contentPane.add(lBanco);
		lBanco.setVisible(false);
		
		lCheque = new JLabel("Cheque");
		lCheque.setBounds(35, 358, 91, 21);
		contentPane.add(lCheque);
		lCheque.setVisible(false);
		
		nroCheque = new JTextField();
		nroCheque.setColumns(10);
		nroCheque.setBounds(98, 359, 194, 20);
		contentPane.add(nroCheque);
		nroCheque.setText("");
		nroCheque.setVisible(false);
		
		lTarjeta = new JLabel("Tarjeta");
		lTarjeta.setBounds(35, 357, 91, 21);
		contentPane.add(lTarjeta);
		lTarjeta.setVisible(false);
		
		nroTarjeta = new JTextField();
		nroTarjeta.setColumns(10);
		nroTarjeta.setBounds(98, 358, 194, 20);
		contentPane.add(nroTarjeta);
		nroTarjeta.setText("0");
		nroTarjeta.setVisible(false);
		
		lVencimiento = new JLabel("Vencimiento");
		lVencimiento.setBounds(358, 359, 91, 21);
		contentPane.add(lVencimiento);
		lVencimiento.setVisible(false);
		
		fechaVto = new JTextField();
		fechaVto.setColumns(10);
		fechaVto.setBounds(432, 358, 103, 20);
		contentPane.add(fechaVto);
		fechaVto.setText("");
		fechaVto.setVisible(false);
		
		lCBU = new JLabel("CBU");
		lCBU.setBounds(35, 358, 91, 21);
		contentPane.add(lCBU);
		lCBU.setVisible(false);
		
		CBU = new JTextField();
		CBU.setColumns(10);
		CBU.setBounds(98, 359, 194, 20);
		CBU.setText("0");
		contentPane.add(CBU);
		CBU.setVisible(false);
		
		//Obengo los nombres de los bancos en Medios
		Vector<String> nombresBancos = Sistema.getInstancia().getBancos();		
		banco = new JComboBox();
		banco.setModel(new DefaultComboBoxModel(nombresBancos));
		banco.setBounds(98, 314, 165, 20);
		contentPane.add(banco);
		banco.setVisible(false);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 166, 631, 2);
		contentPane.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 238, 631, 2);
		contentPane.add(separator_1);
		
		comboMarcas = new JComboBox();
		comboMarcas.setBounds(98, 121, 194, 20);
		contentPane.add(comboMarcas);
		comboMarcas.addItem("Seleccione");
		
		List<String> marcas = Sistema.getInstancia().getMarcas();
		
		for (String m : marcas)
			comboMarcas.addItem(m);
			
	}
	
	private void mostrarCampos(){
		int i = comboPago.getSelectedIndex();
		
		switch (i){
			case 0: //efectivo
				lBanco.setVisible(false);
				banco.setVisible(false);
				lCheque.setVisible(false);
				nroCheque.setVisible(false);
				lTarjeta.setVisible(false);
				nroTarjeta.setVisible(false);
				lCBU.setVisible(false);
				CBU.setVisible(false);
				lVencimiento.setVisible(false);
				fechaVto.setVisible(false);
				break;
				
			case 1: //cheque
				lBanco.setVisible(true);
				banco.setVisible(true);
				lCheque.setVisible(true);
				nroCheque.setVisible(true);
				lTarjeta.setVisible(false);
				nroTarjeta.setVisible(false);
				lCBU.setVisible(false);
				CBU.setVisible(false);
				lVencimiento.setVisible(true);
				fechaVto.setVisible(true);
				break;
				
			case 2: // Tarjeta
				lBanco.setVisible(true);
				banco.setVisible(true);
				lCheque.setVisible(false);
				nroCheque.setVisible(false);
				lTarjeta.setVisible(true);
				nroTarjeta.setVisible(true);
				lCBU.setVisible(false);
				CBU.setVisible(false);
				lVencimiento.setVisible(true);
				fechaVto.setVisible(true);
				break;
				
			case 3: //debitolBanco
				lBanco.setVisible(true);
				banco.setVisible(true);
				lCheque.setVisible(false);
				nroCheque.setVisible(false);
				lTarjeta.setVisible(false);
				nroTarjeta.setVisible(false);
				lCBU.setVisible(true);
				CBU.setVisible(true);
				lVencimiento.setVisible(false);
				fechaVto.setVisible(false);
				break;
				
		}
	}
	
	public void limpiarPantalla(){
		DNI.setText("");
		patente.setText("");
		comboMarcas.setSelectedIndex(0);
		modelo.setText("");
		comboTipo.setSelectedIndex(0);
		comboPago.setSelectedIndex(0);
		banco.setSelectedIndex(0);
		nroCheque.setText("");
		nroTarjeta.setText("0");
		CBU.setText("0");
		fechaVto.setText("");

	}
	
	public boolean vacio(){
		if (DNI.getText().isEmpty() || patente.getText().isEmpty() || comboMarcas.getSelectedItem().toString().equals("Seleccione") ||
				modelo.getText().isEmpty()  )
			return true;
		else
			return false;	
	}
	
	private boolean validarCampos(){
		
		if ( patente.getText().length() <= 10 && modelo.getText().length() <= 20 && nroCheque.getText().length() <= 50 )
			return true;
		else
			return false;
	}
	
	public void close(){
		this.dispose();		
	}
}
