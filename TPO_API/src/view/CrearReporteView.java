package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;

import model.ReciboView;
import model.ReporteView;
import controlador.Sistema;
import java.awt.event.ActionListener;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;


public class CrearReporteView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4203049338995558566L;
	private JPanel contentPane;
	private ReporteView reporte = null;
	private Timestamp fechaDesde = null;
	private Timestamp fechaHasta = null;
	private JComboBox anioHasta;
	private JComboBox mesHasta;
	private JComboBox anioDesde;
	private JComboBox mesDesde;
	private JList lista;
	private JScrollPane scroll; 
	private JTextField cant;
	private JTextField total;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearReporteView frame = new CrearReporteView();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CrearReporteView() {
		setTitle("Creacion de Reporte");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDesde = new JLabel("Desde");
		lblDesde.setBounds(33, 25, 66, 21);
		contentPane.add(lblDesde);
		
		JButton bModificar = new JButton("IMPRIMIR");
		bModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (reporte != null){
					JOptionPane.showMessageDialog(null, "Imprimiendo");
					close();
				}
				else
					JOptionPane.showMessageDialog(null, "No hay datos para imprimir");
				
			}
		});
		bModificar.setBounds(210, 304, 105, 23);
		contentPane.add(bModificar);
		
		JButton bCancelar = new JButton("CERRAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				close();
			}
		});
		bCancelar.setBounds(347, 304, 105, 23);
		contentPane.add(bCancelar);
		
		JButton bBuscar = new JButton("Generar");
		bBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
												
				obtenerFechas();
				
				reporte = Sistema.getInstancia().crearReporteView(fechaDesde, fechaHasta);
				
				if (reporte != null){
					
					DecimalFormat df = new DecimalFormat("#.##");
					df.setRoundingMode(RoundingMode.CEILING);
					
					cant.setText( String.valueOf(reporte.getRecibos().size()) );
					total.setText( String.valueOf( df.format(reporte.getTotal()) ) );
					
										
					String linea = "";
					List<String> listado = new ArrayList<String>();
					for (ReciboView r : reporte.getRecibos()){
						linea = "        " + r.getNroRecibo() + "                             " + r.getFecha().toString().substring(0,19) + 
								"                      " + r.getAbono().getNombre() + 
								"                                  " + df.format(r.getTotal());
						listado.add(linea);
					}
										
					lista.setListData(listado.toArray());
					lista.setBounds(12, 144, 628, 106);
					lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
										
					if (listado.size() > 5 ){
						
						scroll = new JScrollPane(lista);					
						scroll.setBounds(12, 144, 628, 106);
						
						getContentPane().add(scroll, BorderLayout.CENTER);
						
					}

				}
				
				
			}
		});
		bBuscar.setBounds(535, 24, 105, 23);
		contentPane.add(bBuscar);
		
		mesDesde = new JComboBox();
		mesDesde.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		mesDesde.setBounds(87, 25, 46, 21);
		contentPane.add(mesDesde);
		
		anioDesde = new JComboBox();
		anioDesde.setBounds(157, 25, 76, 21);
		contentPane.add(anioDesde);				
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 11, 250, 2);
		contentPane.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(12, 58, 250, 2);
		contentPane.add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setBounds(10, 11, 2, 47);
		contentPane.add(separator_2);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setBounds(261, 13, 2, 47);
		contentPane.add(separator_3);
		
		JLabel lblHasta_1 = new JLabel("Hasta");
		lblHasta_1.setBounds(295, 25, 66, 21);
		contentPane.add(lblHasta_1);
		
		mesHasta = new JComboBox();
		mesHasta.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		mesHasta.setBounds(349, 25, 46, 21);
		contentPane.add(mesHasta);
		
		anioHasta = new JComboBox();
		anioHasta.setBounds(419, 25, 76, 21);
		contentPane.add(anioHasta);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBounds(274, 11, 250, 2);
		contentPane.add(separator_4);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setBounds(274, 58, 250, 2);
		contentPane.add(separator_5);
		
		JSeparator separator_6 = new JSeparator();
		separator_6.setOrientation(SwingConstants.VERTICAL);
		separator_6.setBounds(272, 11, 2, 47);
		contentPane.add(separator_6);
		
		JSeparator separator_7 = new JSeparator();
		separator_7.setOrientation(SwingConstants.VERTICAL);
		separator_7.setBounds(523, 13, 2, 47);
		contentPane.add(separator_7);

		int anio = Calendar.getInstance().get(Calendar.YEAR);	
		for (int i=0;i<=10;i++){										
			anioDesde.addItem(  Integer.toString((anio-i)) );
			anioHasta.addItem( Integer.toString((anio-i)) );			
		}
		
		mesHasta.setSelectedIndex(11);
		
		lista = new JList();
		lista.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lista.setBounds(12, 144, 628, 106);
		contentPane.add(lista);
		
		JLabel lblRecibo = new JLabel("RECIBO");
		lblRecibo.setBounds(37, 115, 53, 21);
		contentPane.add(lblRecibo);
		
		JLabel lblFecha = new JLabel("FECHA");
		lblFecha.setBounds(213, 115, 53, 21);
		contentPane.add(lblFecha);
		
		JLabel lblAbono = new JLabel("ABONO");
		lblAbono.setBounds(401, 115, 53, 21);
		contentPane.add(lblAbono);
		
		JLabel lblTotal = new JLabel("TOTAL");
		lblTotal.setBounds(567, 115, 53, 21);
		contentPane.add(lblTotal);
		
		JSeparator separator_8 = new JSeparator();
		separator_8.setBounds(12, 138, 628, 2);
		contentPane.add(separator_8);
		
		JSeparator separator_9 = new JSeparator();
		separator_9.setBounds(12, 113, 628, 2);
		contentPane.add(separator_9);
		
		JLabel lblCantidadDeRecibos = new JLabel("Cantidad de recibos");
		lblCantidadDeRecibos.setBounds(21, 77, 123, 21);
		contentPane.add(lblCantidadDeRecibos);
		
		cant = new JTextField();
		cant.setEditable(false);
		cant.setBounds(143, 77, 86, 22);
		contentPane.add(cant);
		cant.setColumns(10);
		
		total = new JTextField();
		total.setFont(new Font("Tahoma", Font.BOLD, 12));
		total.setEditable(false);
		total.setColumns(10);
		total.setBounds(538, 261, 86, 22);
		contentPane.add(total);
		
		JLabel label = new JLabel("$");
		label.setBounds(523, 262, 15, 21);
		contentPane.add(label);
		
	}
	
	public void obtenerFechas(){
		
		String dDesde = "1";
		String mDesde = mesDesde.getSelectedItem().toString();
		String aDesde = anioDesde.getSelectedItem().toString();
		String fd = aDesde + "-" + mDesde + "-" + dDesde + " 00:00:00";
		fechaDesde = Timestamp.valueOf(fd);
		
		String dHasta = "31";
		String mHasta = mesHasta.getSelectedItem().toString();
		String aHasta = anioHasta.getSelectedItem().toString();
		String fh = aHasta + "-" + mHasta + "-" + dHasta + " 23:59:59";
		fechaHasta = Timestamp.valueOf(fh);
	}
	
	public boolean vacio(){
		if (fechaDesde == null || fechaHasta == null)
			return true;
		else
			return false;
	}
		
	public void close(){
		this.dispose();
	}
}
;