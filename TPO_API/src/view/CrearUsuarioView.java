package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import controlador.Sistema;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Arrays;

import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import model.Permiso;
import model.UsuarioView;

public class CrearUsuarioView extends JFrame {

	private static final long serialVersionUID = -4956625203973577699L;
	private JPanel contentPane;
	private JTextField alias;
	private JTextField nombreReal;
	private JPasswordField password1;
	private JPasswordField password2;
	private JComboBox permiso;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearUsuarioView frame = new CrearUsuarioView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CrearUsuarioView() {
		setTitle("Creacion de Ususario");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("Usuario");
		lblDni.setBounds(35, 39, 66, 21);
		contentPane.add(lblDni);
		
		alias = new JTextField();
		alias.setColumns(10);
		alias.setBounds(154, 39, 194, 20);
		contentPane.add(alias);
		
		JLabel lblNombre = new JLabel("Password");
		lblNombre.setBounds(35, 83, 66, 21);
		contentPane.add(lblNombre);
		
		nombreReal = new JTextField();
		nombreReal.setColumns(10);
		nombreReal.setBounds(154, 168, 194, 20);
		contentPane.add(nombreReal);
		
		JLabel lblDomicilio = new JLabel("Nombre Real");
		lblDomicilio.setBounds(35, 168, 141, 21);
		contentPane.add(lblDomicilio);
		
		JLabel lblTelefono = new JLabel("Permiso");
		lblTelefono.setBounds(35, 216, 66, 21);
		contentPane.add(lblTelefono);
		
		JButton bCrear = new JButton("CREAR");
		bCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !vacio() ){
					
					if ( validarPasswordIguales() ){
						
						if ( validarPasswordSize() ){
							
							if ( validarCampos() ){
								
								String p = String.valueOf(password1.getPassword());
								UsuarioView usuario = new UsuarioView(alias.getText(),p,nombreReal.getText(),Permiso.valueOf(permiso.getSelectedItem().toString()),true);
								boolean exito = Sistema.getInstancia().crearUsuario(usuario);
								
								if (exito){
									close();					
								}
								else
									JOptionPane.showMessageDialog(null, "Usuario ya existe");
								
							}
							else
								JOptionPane.showMessageDialog(null, "Excedi� el limite de caracteres de alguno de los campos");
							
						}
						else
							JOptionPane.showMessageDialog(null, "La contrase�a debe contener 5 caracteres o m�s");
					}
					else
						JOptionPane.showMessageDialog(null, "Las contrase�as no coinciden");
				}
				else
					JOptionPane.showMessageDialog(null, "Faltan ingresar datos");
			}
		});
		bCrear.setBounds(203, 295, 105, 23);
		contentPane.add(bCrear);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarPantalla();
				close();
			}
		});
		bCancelar.setBounds(365, 295, 105, 23);
		contentPane.add(bCancelar);
		
		password1 = new JPasswordField();
		password1.setBounds(154, 82, 194, 20);
		contentPane.add(password1);
		
		permiso = new JComboBox();
		permiso.setModel( new DefaultComboBoxModel(new String[] {"Administrador", "Operador"}) );
		permiso.setBounds(155, 215, 193, 21);
		contentPane.add(permiso);		
		
		JLabel lblReingresePassword = new JLabel("Reingrese Password");
		lblReingresePassword.setBounds(35, 125, 141, 21);
		contentPane.add(lblReingresePassword);
		
		password2 = new JPasswordField();
		password2.setBounds(154, 124, 194, 20);
		contentPane.add(password2);
	}
	
	public void limpiarPantalla(){
		alias.setText("");
		password1.setText("");
		nombreReal.setText("");
	}
	
	public boolean vacio(){
		if (alias.getText().isEmpty() || nombreReal.getText().isEmpty() || password1.getPassword().length == 0 || password2.getPassword().length == 0 )
			return true;
		else
			return false;
	}
	
	private boolean validarPasswordIguales(){
		
		if ( Arrays.equals( password1.getPassword(), password2.getPassword()) )
			return true;
		else
			return false;
	}
	
	private boolean validarPasswordSize(){
		
		if (password1.getPassword().length >=5 && password2.getPassword().length >= 5)
			return true;
		else
			return false;
	}
	
	private boolean validarCampos(){
		
		if ( alias.getText().length() <= 30 && password1.getPassword().length <= 30)
			return true;
		else
			return false;
		
	}
	
	
	public void close(){
		this.dispose();
	}
}
