package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

import model.AbonoView;
import controlador.Sistema;

import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JSeparator;

public class EliminarAbonoView extends JFrame {

	private static final long serialVersionUID = 5785933748604560028L;
	private JPanel contentPane;
	private JLabel precio;
	private JLabel lblHoras;
	private JLabel horas;
	private JLabel lblDescuento;
	private JLabel descuento;
	private JComboBox abonos;
	private List<AbonoView> abonosView;
	private JSeparator separator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarAbonoView frame = new EliminarAbonoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarAbonoView() {
		setTitle("Eliminacion de Abono");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("Nombre");
		lblDni.setBounds(35, 39, 66, 21);
		contentPane.add(lblDni);
		
		JLabel lblTelefono = new JLabel("Precio");
		lblTelefono.setBounds(35, 157, 66, 21);
		contentPane.add(lblTelefono);
		
		JButton bEliminar = new JButton("ELIMINAR");
		bEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				
				if ( !abonos.getSelectedItem().equals("Seleccione") ){			
					boolean exito = Sistema.getInstancia().eliminarAbono( abonos.getSelectedItem().toString() );
					
					if (exito){
						close();						
					}
					else
						JOptionPane.showMessageDialog(null, "Existen contratos activos con el abono");
					
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un abono");
			}
		});
		bEliminar.setBounds(198, 280, 105, 23);
		contentPane.add(bEliminar);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(360, 280, 105, 23);
		contentPane.add(bCancelar);
		
		precio = new JLabel("");
		precio.setBounds(111, 157, 66, 21);
		contentPane.add(precio);
		
		lblHoras = new JLabel("Horas");
		lblHoras.setBounds(35, 111, 66, 21);
		contentPane.add(lblHoras);
		
		horas = new JLabel("");
		horas.setBounds(111, 111, 66, 21);
		contentPane.add(horas);
		
		lblDescuento = new JLabel("Descuento");
		lblDescuento.setBounds(35, 200, 66, 21);
		contentPane.add(lblDescuento);
		
		descuento = new JLabel("");
		descuento.setBounds(111, 200, 66, 21);
		contentPane.add(descuento);
		
		abonos = new JComboBox();		
		abonos.setBounds(111, 39, 173, 20);
		contentPane.add(abonos);
		abonos.addItem("Seleccione");
		
		separator = new JSeparator();
		separator.setBounds(33, 89, 581, 2);
		contentPane.add(separator);
		
		abonosView  = Sistema.getInstancia().buscarAbonosView();
		
		for (AbonoView a : abonosView)
			abonos.addItem( a.getNombre() );
		
		abonos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !abonos.getSelectedItem().equals("Seleccione") ){
					
					for (AbonoView a : abonosView)
						if ( a.getNombre().equals(abonos.getSelectedItem().toString()) ){
							
							horas.setText( String.valueOf(a.getHoras()) );
							precio.setText( String.valueOf(a.getPrecio()) );
							descuento.setText( String.valueOf(a.getDescuento()) );
							break;

						}
					
				}
			}
		});
		
		
	}
	
	public void limpiarPantalla(){	
		abonos.setSelectedIndex(0);
		horas.setText("");
		precio.setText("");
		descuento.setText("");		
	}
	
	public void close(){
		this.dispose();
	}
}
