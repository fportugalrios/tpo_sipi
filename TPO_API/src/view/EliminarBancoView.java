package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import model.BancoView;
import controlador.Sistema;
import javax.swing.JComboBox;
import javax.swing.JSeparator;

public class EliminarBancoView extends JFrame {

	private static final long serialVersionUID = -6254505122924581503L;
	
	private JPanel contentPane;
	private JLabel url;
	private JLabel archivo;
	private JComboBox bancos;
	List<BancoView> bancosView;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarBancoView frame = new EliminarBancoView();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarBancoView() {
		setTitle("Eliminacion de Banco");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblContrato = new JLabel("Banco");
		lblContrato.setBounds(41, 40, 91, 21);
		contentPane.add(lblContrato);
		
		JLabel lblModelo_1 = new JLabel("URL");
		lblModelo_1.setBounds(41, 114, 66, 21);
		contentPane.add(lblModelo_1);
		
		JButton bEliminar = new JButton("ELIMINAR");
		bEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !bancos.getSelectedItem().equals("Seleccione") ){
					
					boolean exito = Sistema.getInstancia().eliminarBanco( bancos.getSelectedItem().toString() );
					
					if (exito){
						close();						
					}
					else
						JOptionPane.showMessageDialog(null, "Existen contratos activos con el banco");
					
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un banco");
				
			}
		});
		bEliminar.setBounds(192, 298, 105, 23);
		contentPane.add(bEliminar);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(354, 298, 105, 23);
		contentPane.add(bCancelar);
		
		JLabel lblCheque = new JLabel("Archivo");
		lblCheque.setBounds(41, 164, 91, 21);
		contentPane.add(lblCheque);
		
		url = new JLabel("");
		url.setBounds(101, 114, 321, 21);
		contentPane.add(url);
		
		archivo = new JLabel("");
		archivo.setBounds(101, 164, 326, 21);
		contentPane.add(archivo);
		
		bancos = new JComboBox();
		
		bancos.setBounds(100, 42, 167, 20);
		contentPane.add(bancos);
		bancos.addItem("Seleccione");
		
		JSeparator separator = new JSeparator();
		separator.setBounds(38, 91, 581, 2);
		contentPane.add(separator);
		
		bancosView = Sistema.getInstancia().buscarBancosView();
		
		for (BancoView b : bancosView)
			bancos.addItem( b.getBanco() );
		
		bancos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !bancos.getSelectedItem().equals("Seleccione") ){
					
					for (BancoView b : bancosView)
						if ( b.getBanco().equals( bancos.getSelectedItem().toString() )){
							
							url.setText( b.getUrlConexion());
							archivo.setText( b.getNombreArchivo() );
						}
					
				}
			}
		});
		
	}
		
	public void limpiarPantalla(){
		url.setText("");
		archivo.setText("");
	}
	
	public void close(){
		this.dispose();
	}
	
}
