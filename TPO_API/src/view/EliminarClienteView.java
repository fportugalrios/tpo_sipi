package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

import model.ClienteView;
import controlador.Sistema;

import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JSeparator;

public class EliminarClienteView extends JFrame {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4830464471551821299L;
	private JPanel contentPane;
	private JLabel nombre;
	private JLabel domicilio;
	private JLabel telefono;
	private JComboBox clientes;
	private List<ClienteView> clientesView;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarClienteView frame = new EliminarClienteView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarClienteView() {
		setResizable(false);
		setTitle("Eliminacion de Cliente");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("DNI");
		lblDni.setBounds(38, 42, 66, 21);
		contentPane.add(lblDni);
		
		JButton bEliminar = new JButton("ELIMINAR");
		bEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				if ( !clientes.getSelectedItem().equals("Seleccione") ){
					
					int dni = Integer.valueOf( clientes.getSelectedItem().toString() );				
					boolean exito = Sistema.getInstancia().eliminarCliente(dni);
					
					if (exito){
						close();						
					}
					else
						JOptionPane.showMessageDialog(null, "Cliente con contratos activos");
									
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un DNI");

			}
		});
		bEliminar.setBounds(211, 282, 105, 23);
		contentPane.add(bEliminar);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(373, 282, 105, 23);
		contentPane.add(bCancelar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(38, 118, 66, 21);
		contentPane.add(lblNombre);
		
		JLabel lblDomicilio = new JLabel("Domicilio");
		lblDomicilio.setBounds(38, 162, 66, 21);
		contentPane.add(lblDomicilio);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(38, 210, 66, 21);
		contentPane.add(lblTelefono);
		
		nombre = new JLabel("");
		nombre.setBounds(103, 118, 194, 21);
		contentPane.add(nombre);
		
		domicilio = new JLabel("");
		domicilio.setBounds(103, 162, 194, 21);
		contentPane.add(domicilio);
		
		telefono = new JLabel("");
		telefono.setBounds(103, 210, 194, 21);
		contentPane.add(telefono);
		
		clientes = new JComboBox();
		
		clientes.setBounds(103, 42, 194, 20);
		contentPane.add(clientes);
		clientes.addItem("Seleccione");
		
		JSeparator separator = new JSeparator();
		separator.setBounds(38, 94, 581, 2);
		contentPane.add(separator);
		
		clientesView = Sistema.getInstancia().buscarClientesView();
		
		for (ClienteView c : clientesView)
			clientes.addItem( String.valueOf(c.getDNI()) );
		
		clientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !clientes.getSelectedItem().equals("Seleccione") ){
					
					for (ClienteView c : clientesView)
						if ( c.getDNI() == Integer.valueOf(clientes.getSelectedItem().toString()) ){
							
							nombre.setText( c.getNombre() );
							domicilio.setText( c.getDomicilio() );
							telefono.setText( String.valueOf( c.getTel()) );
							break;
						}
					
				}
			}
		});
		
		
	}
	
	public void limpiarPantalla(){
		nombre.setText("");
		domicilio.setText("");
		telefono.setText("");
	}
	
	public void close(){
		this.dispose();
	}

}
