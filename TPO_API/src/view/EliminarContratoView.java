package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import model.ContratoView;
import controlador.Sistema;
import javax.swing.JComboBox;
import javax.swing.JSeparator;

public class EliminarContratoView extends JFrame {

	private static final long serialVersionUID = -7305158745859800293L;
	
	private JPanel contentPane;
	private JLabel patente;
	private JLabel marca;
	private JLabel modelo;
	private JLabel tipo;
	List<ContratoView> contratosView;
	private JComboBox contratos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarContratoView frame = new EliminarContratoView();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarContratoView() {
		setTitle("Eliminacion de Contrato");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblContrato = new JLabel("Contrato");
		lblContrato.setBounds(37, 34, 91, 21);
		contentPane.add(lblContrato);
		
		JLabel lblModelo = new JLabel("Patente");
		lblModelo.setBounds(37, 100, 66, 21);
		contentPane.add(lblModelo);
		
		JLabel lblMarca = new JLabel("Modelo");
		lblMarca.setBounds(37, 189, 66, 21);
		contentPane.add(lblMarca);
		
		JLabel lblModelo_1 = new JLabel("Marca");
		lblModelo_1.setBounds(37, 145, 66, 21);
		contentPane.add(lblModelo_1);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(37, 234, 66, 21);
		contentPane.add(lblTipo);
		
		JButton bEliminar = new JButton("ELIMINAR");
		bEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !contratos.getSelectedItem().equals("Seleecione")){
					
					int nro = Integer.parseInt( contratos.getSelectedItem().toString() );
					boolean exito = Sistema.getInstancia().eliminarContrato( nro );
					
					if (exito){
						close();						
					}
					else
						JOptionPane.showMessageDialog(null, "No se puede eliminar el contrato");						
					
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un contrato");
				
			}
		});
		bEliminar.setBounds(203, 295, 105, 23);
		contentPane.add(bEliminar);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(365, 295, 105, 23);
		contentPane.add(bCancelar);
		
		patente = new JLabel("");
		patente.setBounds(104, 100, 186, 21);
		contentPane.add(patente);
		
		marca = new JLabel("");
		marca.setBounds(104, 145, 186, 21);
		contentPane.add(marca);
		
		modelo = new JLabel("");
		modelo.setBounds(104, 189, 186, 21);
		contentPane.add(modelo);
		
		tipo = new JLabel("");
		tipo.setBounds(104, 234, 186, 21);
		contentPane.add(tipo);
		
		contratos = new JComboBox();
		
		contratos.setBounds(104, 34, 125, 20);
		contentPane.add(contratos);
		contratos.addItem("Seleccione");
		
		JSeparator separator = new JSeparator();
		separator.setBounds(37, 79, 581, 2);
		contentPane.add(separator);
		
		contratosView = Sistema.getInstancia().buscarContratosView();
		
		for (ContratoView c : contratosView)
			contratos.addItem( String.valueOf(c.getNroContrato())  );
		
		contratos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !contratos.getSelectedItem().equals("Seleccione") ){

					for (ContratoView c : contratosView)
						if ( c.getNroContrato() == Integer.valueOf(contratos.getSelectedItem().toString()) ){
							
							patente.setText( c.getAuto().getPatente() );
							marca.setText( c.getAuto().getMarca() );
							modelo.setText(c.getAuto().getModelo() );
							tipo.setText( c.getAuto().getTipo() );
							break;
							
						}

				}
			}
		});
		
		
		
	}
	
	
	public void limpiarPantalla(){
		patente.setText("");
		modelo.setText("");
		marca.setText("");
		tipo.setText("");		
	}
	
	public void close(){
		this.dispose();		
	}
}
