package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

import model.UsuarioView;
import controlador.Sistema;

import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JSeparator;

public class EliminarUsuarioView extends JFrame {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4830464471551821299L;
	private JPanel contentPane;
	private JLabel nombreReal;
	private JLabel permiso;
	private JComboBox usuarios;
	private List<UsuarioView> usuariosView;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarUsuarioView frame = new EliminarUsuarioView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarUsuarioView() {
		setResizable(false);
		setTitle("Eliminacion de Usuario");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("Alias");
		lblDni.setBounds(38, 39, 66, 21);
		contentPane.add(lblDni);
		
		JButton bEliminar = new JButton("ELIMINAR");
		bEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !usuarios.getSelectedItem().equals("Seleccione") ){
								
					boolean exito = Sistema.getInstancia().eliminarUsuario( usuarios.getSelectedItem().toString() );
					
					if (exito){
						close();
					}
					else
						JOptionPane.showMessageDialog(null, "No se puede eliminar el usuario");
						
					
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un usuario");
				
				
			}
		});
		bEliminar.setBounds(211, 274, 105, 23);
		contentPane.add(bEliminar);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(373, 274, 105, 23);
		contentPane.add(bCancelar);
		
		JLabel lblNombre = new JLabel("Nombre real");
		lblNombre.setBounds(38, 120, 105, 21);
		contentPane.add(lblNombre);
		
		JLabel lblDomicilio = new JLabel("Permiso");
		lblDomicilio.setBounds(38, 164, 66, 21);
		contentPane.add(lblDomicilio);
		
		nombreReal = new JLabel("");
		nombreReal.setBounds(127, 120, 194, 21);
		contentPane.add(nombreReal);
		
		permiso = new JLabel("");
		permiso.setBounds(127, 164, 194, 21);
		contentPane.add(permiso);
		
		usuarios = new JComboBox();
		
		usuarios.setBounds(86, 39, 155, 20);
		contentPane.add(usuarios);
		usuarios.addItem("Seleccione");
		
		JSeparator separator = new JSeparator();
		separator.setBounds(38, 91, 581, 2);
		contentPane.add(separator);
		
		usuariosView = Sistema.getInstancia().buscarUsuariosView();
		
		for (UsuarioView u : usuariosView)
			usuarios.addItem( u.getAlias() );
		
		usuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !usuarios.getSelectedItem().equals("Seleccione") ){
					
					for (UsuarioView u : usuariosView)
						if (u.getAlias().equals(usuarios.getSelectedItem().toString()) ){
							
							nombreReal.setText( u.getNombreReal() );
							permiso.setText( u.getPermiso().toString() );
							break;
							
						}
				}
				
			}
		});
		
		
	}
	
	public void limpiarPantalla(){
		nombreReal.setText("");
		permiso.setText("");
	}
	
	public void close(){
		this.dispose();
	}

}
