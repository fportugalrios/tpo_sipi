package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import controlador.Sistema;
import model.Permiso;

import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

public class Login extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField alias;
	private static Login instancia;
	private JPasswordField password;
	private Permiso sesion = null;
	private JLabel lblNewLabel;
	private JSeparator separator_1;
	private JSeparator separator_2;
	private JSeparator separator_3;
	
	
	public static Login getInstancia(){
		if (instancia == null)
			instancia = new Login();
		return instancia;
	}
	



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		
		Sistema.getInstancia();
		
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		setResizable(false);
		setTitle("Alquiler de Cocheras");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPatente = new JLabel("Usuario");
		lblPatente.setBounds(293, 89, 50, 21);
		contentPane.add(lblPatente);
		
		alias = new JTextField();
		alias.setBounds(244, 110, 143, 20);
		contentPane.add(alias);
		alias.setColumns(10);
		
		JButton bEntrada = new JButton("Ingresar");
		bEntrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if ( !vacio() ){
					
					sesion = Sistema.getInstancia().validarUsuario(alias.getText(),password.getPassword());
					
					limpiarPantalla();
					
					if ( sesion.equals(Permiso.Master) || sesion.equals(Permiso.Administrador) ){
						close();
						MenuAdm.getInstancia().setVisible(true);
					}
					else
						if (sesion.equals(Permiso.Operador)){
							close();
							MenuOp.getInstancia().setVisible(true);
						}
						else
							JOptionPane.showMessageDialog(null, "Usuario y/o contraseņa incorrecta");
					
				}
				else
					JOptionPane.showMessageDialog(null, "Ingrese usuario y/o contraseņa");

			}
			
		});
		bEntrada.setBounds(263, 233, 105, 23);
		contentPane.add(bEntrada);

		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setBounds(284, 158, 66, 21);
		contentPane.add(lblContrasea);
		
		password = new JPasswordField();
		password.setToolTipText("Contrase\u00F1a");
		password.setBounds(244, 179, 143, 20);
		contentPane.add(password);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("src\\img\\login.png"));
		lblNewLabel.setBounds(565, 24, 50, 50);
		contentPane.add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(55, 45, 536, 21);
		contentPane.add(separator);
		
		separator_1 = new JSeparator();
		separator_1.setBounds(55, 297, 536, 21);
		contentPane.add(separator_1);
		
		separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setBounds(53, 46, 9, 251);
		contentPane.add(separator_2);
		
		separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setBounds(591, 46, 9, 251);
		contentPane.add(separator_3);
	}
	
	public void close(){
		this.dispose();
	}
	
	public void limpiarPantalla(){
		alias.setText("");
		password.setText("");		
	}
	
	public Permiso getSesion(){
		return sesion;
	}
	
	
	public boolean vacio(){
		
		if (alias.getText().isEmpty() || password.getPassword().length == 0 ){
			return true;
		}
		else 
			return false;
	}
}
