package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Timestamp;

import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controlador.Sistema;
import model.ContratoView;

import javax.swing.JSeparator;
import javax.swing.ImageIcon;

public class MenuAdm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField patente;
	private static MenuAdm instancia;
	
	public static MenuAdm getInstancia(){
		if (instancia == null)
			instancia = new MenuAdm();
		return instancia;
	}


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuAdm frame = new MenuAdm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuAdm() {
		
		Sistema.getInstancia();
		
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		setResizable(false);
		setTitle("Alquiler de Cocheras");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 651, 21);
		contentPane.add(menuBar);
		
		JMenu mnInicio = new JMenu("Inicio");
		menuBar.add(mnInicio);
		
		JMenuItem mntmCerrarSesion = new JMenuItem("Cerrar sesi\u00F3n");
		mntmCerrarSesion.setIcon(new ImageIcon("src\\img\\key.png"));
		mntmCerrarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
				Login.getInstancia().setVisible(true);
			}
		});
		mnInicio.add(mntmCerrarSesion);
		
		JMenu mnNewMenu = new JMenu("Clientes");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Crear Cliente");
		mntmNewMenuItem.setIcon(new ImageIcon("src\\img\\crear.png"));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				CrearClienteView.getInstancia().setVisible(true);
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Modificar Cliente");
		mntmNewMenuItem_1.setIcon(new ImageIcon("src\\img\\modificar.png"));
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarClienteView vModificarCliente = new ModificarClienteView();
				vModificarCliente.setVisible(true);
			}
		});
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Eliminar Cliente");
		mntmNewMenuItem_2.setIcon(new ImageIcon("src\\img\\eliminar.png"));
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarClienteView vEliminarCliente = new EliminarClienteView();
				vEliminarCliente.setVisible(true);
			}
		});
		mnNewMenu.add(mntmNewMenuItem_2);
		
		JMenu mnNewMenu_1 = new JMenu("Contratos");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_8 = new JMenuItem("Crear Contrato");
		mntmNewMenuItem_8.setIcon(new ImageIcon("src\\img\\crear.png"));
		mntmNewMenuItem_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CrearContratoView vCrearContrato = new CrearContratoView();
				vCrearContrato.setVisible(true);
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_8);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Eliminar Contrato");
		mntmNewMenuItem_3.setIcon(new ImageIcon("src\\img\\eliminar.png"));
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarContratoView vEliminarContrato = new EliminarContratoView();
				vEliminarContrato.setVisible(true);

			}
		});
		
		JMenuItem mntmConsultarContratos = new JMenuItem("Consultar Contrato");
		mntmConsultarContratos.setIcon(new ImageIcon("src\\img\\buscar.png"));
		mntmConsultarContratos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultarContratoView vConsultar = new ConsultarContratoView();
				vConsultar.setVisible(true);
			}
		});
		mnNewMenu_1.add(mntmConsultarContratos);
		mnNewMenu_1.add(mntmNewMenuItem_3);
		
		JMenu mnNewMenu_2 = new JMenu("Abonos");
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Crear Abono");
		mntmNewMenuItem_4.setIcon(new ImageIcon("src\\img\\crear.png"));
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CrearAbonoView vCrearAbono = new CrearAbonoView();
				vCrearAbono.setVisible(true);
			}
		});
		mnNewMenu_2.add(mntmNewMenuItem_4);
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Modificar Abono");
		mntmNewMenuItem_5.setIcon(new ImageIcon("src\\img\\modificar.png"));
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarAbonoView vModificarAbono = new ModificarAbonoView();
				vModificarAbono.setVisible(true);

			}
		});
		mnNewMenu_2.add(mntmNewMenuItem_5);
		
		JMenuItem mntmNewMenuItem_6 = new JMenuItem("Eliminar Abono");
		mntmNewMenuItem_6.setIcon(new ImageIcon("src\\img\\eliminar.png"));
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarAbonoView vEliminarAbono = new EliminarAbonoView();
				vEliminarAbono.setVisible(true);

			}
		});
		mnNewMenu_2.add(mntmNewMenuItem_6);
		
		JMenu mnNewMenu_3 = new JMenu("Bancos");
		menuBar.add(mnNewMenu_3);
		
		JMenuItem mntmCrearMedio = new JMenuItem("Crear Banco");
		mntmCrearMedio.setIcon(new ImageIcon("src\\img\\crear.png"));
		mntmCrearMedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CrearBancoView vCrearMedio = new CrearBancoView();
				vCrearMedio.setVisible(true);

			}
		});
		mnNewMenu_3.add(mntmCrearMedio);
		
		JMenuItem mntmNewMenuItem_7 = new JMenuItem("Eliminar Banco");
		mntmNewMenuItem_7.setIcon(new ImageIcon("src\\img\\eliminar.png"));
		mntmNewMenuItem_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarBancoView vEliminarMedio = new EliminarBancoView();
				vEliminarMedio.setVisible(true);
			}
		});
		mnNewMenu_3.add(mntmNewMenuItem_7);
				
				JMenu mnNewMenu_4 = new JMenu("Cobranzas");
				menuBar.add(mnNewMenu_4);
				
				JMenuItem mntmNewMenuItem_9 = new JMenuItem("Cobrar contrato");
				mntmNewMenuItem_9.setIcon(new ImageIcon("src\\img\\cobrar.png"));
				mntmNewMenuItem_9.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						CobranzaView vCobranza = new CobranzaView();
						vCobranza.setVisible(true);
					}
				});
				mnNewMenu_4.add(mntmNewMenuItem_9);
				
				JMenu mnNewMenu_5 = new JMenu("Reportes");
				menuBar.add(mnNewMenu_5);
				
				JMenuItem mntmNewMenuItem_10 = new JMenuItem("Crear Reporte");
				mntmNewMenuItem_10.setIcon(new ImageIcon("src\\img\\reporte.png"));
				mntmNewMenuItem_10.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						CrearReporteView vReporte = new CrearReporteView();
						vReporte.setVisible(true);
					}
				});
				mnNewMenu_5.add(mntmNewMenuItem_10);
				
				JMenu mnCochera = new JMenu("Cocheras");
				menuBar.add(mnCochera);
		
		JMenuItem mntmModificarCochera = new JMenuItem("Modificar cochera");
		mntmModificarCochera.setIcon(new ImageIcon("src\\img\\modificar.png"));
		mntmModificarCochera.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
						ModificarCocheraView vModificarCochera = new ModificarCocheraView();
						vModificarCochera.setVisible(true);
					}
				});
		mnCochera.add(mntmModificarCochera);
				
				JMenu mnUsuarios = new JMenu("Usuarios");
				menuBar.add(mnUsuarios);
				
				JMenuItem mntmCrearUsuario = new JMenuItem("Crear Usuario");
				mntmCrearUsuario.setIcon(new ImageIcon("src\\img\\crear.png"));
				mntmCrearUsuario.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						CrearUsuarioView vCrearUsuario = new CrearUsuarioView();
						vCrearUsuario.setVisible(true);

					}
				});
				mnUsuarios.add(mntmCrearUsuario);
				
				JMenuItem mntmEliminarUsuario = new JMenuItem("Eliminar Usuario");
				mntmEliminarUsuario.setIcon(new ImageIcon("src\\img\\eliminar.png"));
				mntmEliminarUsuario.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						EliminarUsuarioView vEliminarUsuario = new EliminarUsuarioView();
						vEliminarUsuario.setVisible(true);

					}
				});
				mnUsuarios.add(mntmEliminarUsuario);
		
		JLabel lblPatente = new JLabel("PATENTE");
		lblPatente.setBounds(301, 68, 66, 21);
		contentPane.add(lblPatente);
		
		patente = new JTextField();
		patente.setHorizontalAlignment(SwingConstants.CENTER);
		patente.setBounds(228, 119, 194, 20);
		contentPane.add(patente);
		patente.setColumns(10);
		
		JButton bEntrada = new JButton("");
		bEntrada.setIcon(new ImageIcon("src\\img\\entrada.png"));
		bEntrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (!patente.getText().isEmpty()){
					
					String pat = patente.getText();
					
					boolean existePatente = Sistema.getInstancia().existePatente(pat);
					
					if ( existePatente ){
						
						boolean exito = Sistema.getInstancia().bloquearCochera(pat);
						
						limpiarCampos();
						
						if (exito){
							ContratoView contrato = Sistema.getInstancia().buscarContratoPatenteView(pat);
							String extension = "";
							String piso = " (P" + String.valueOf( contrato.getCochera().getPiso() ) + ")";
							if ( contrato.getCochera().getExtension() != -1)
								extension = " y " + String.valueOf( contrato.getCochera().getExtension() );
							
							String msg = "ENTRADA ACEPTADA - COCHERA " + contrato.getCochera().getNroCochera() + extension + piso;
							
							JOptionPane.showMessageDialog(null, msg);
						}							
						else
							JOptionPane.showMessageDialog(null, "ENTRADA NO ACEPTADA");
					}
					else{
						limpiarPantalla();
						CrearContratoView vCrearContrato = new CrearContratoView();
						vCrearContrato.setVisible(true); 
					}
				}

			}
			
		});
		bEntrada.setBounds(160, 184, 105, 74);
		contentPane.add(bEntrada);
		
		JButton bSalida = new JButton("");
		bSalida.setIcon(new ImageIcon("src\\img\\salida.png"));
		bSalida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (!vacio()){
					
					ContratoView contrato = Sistema.getInstancia().buscarContratoPatenteView(patente.getText());					
					java.util.Date date= new java.util.Date();
					Timestamp f = new Timestamp(date.getTime());
										
					if ( contrato != null ){
						
						boolean desbloqueo = Sistema.getInstancia().desbloquearCochera(patente.getText());
						
						if ( desbloqueo ){
							
							boolean vencido = Sistema.getInstancia().esContratoVencido( patente.getText() , f);
							
							String extension = "";
							String piso = " (P" + String.valueOf( contrato.getCochera().getPiso() ) + ")";
							if ( contrato.getCochera().getExtension() != -1)
								extension = " y " + String.valueOf( contrato.getCochera().getExtension() );
							
							String msg = "SALIDA ACEPTADA - COCHERA " + contrato.getCochera().getNroCochera() + extension + piso;
							
							JOptionPane.showMessageDialog(null, msg);
									
							if ( !vencido ){
								limpiarCampos();				
							}
							else{
								if ( vencido && !contrato.estaPago() ){
									CobrarView vCobrar = new CobrarView( patente.getText() );
									vCobrar.setVisible(true);
									limpiarCampos();
								}
								else
									Sistema.getInstancia().eliminarContrato(contrato.getNroContrato());
								
							}
							
						}
						else
							JOptionPane.showMessageDialog(null, "SALIDA NO ACEPTADA");															
						
						
							
					}
					else{
						limpiarPantalla();
						if ( contrato == null) 
							JOptionPane.showMessageDialog(null, "PATENTE INEXISTENTE");
					}
				}
			
				
			}
		});
		bSalida.setBounds(386, 184, 105, 74);
		contentPane.add(bSalida);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(103, 62, 444, 8);
		contentPane.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(103, 90, 444, 8);
		contentPane.add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(103, 166, 444, 8);
		contentPane.add(separator_2);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(103, 295, 444, 8);
		contentPane.add(separator_3);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setOrientation(SwingConstants.VERTICAL);
		separator_4.setBounds(101, 63, 14, 232);
		contentPane.add(separator_4);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setOrientation(SwingConstants.VERTICAL);
		separator_5.setBounds(548, 63, 14, 232);
		contentPane.add(separator_5);
		
		JSeparator separator_6 = new JSeparator();
		separator_6.setOrientation(SwingConstants.VERTICAL);
		separator_6.setBounds(323, 166, 14, 130);
		contentPane.add(separator_6);
		
		JLabel lblEntrada = new JLabel("ENTRADA");
		lblEntrada.setBounds(186, 263, 56, 21);
		contentPane.add(lblEntrada);
		
		JLabel lblSalida = new JLabel("SALIDA");
		lblSalida.setBounds(419, 263, 48, 21);
		contentPane.add(lblSalida);
	}
	
	public void close(){
		this.dispose();
	}
	
	public void limpiarPantalla(){
		patente.setText("");
		
	}
	
	public String getPatente(){
		return this.patente.getText();
	}
	

	public void limpiarCampos(){
		patente.setText("");
	}
	
	public boolean vacio(){
		if (patente.getText().isEmpty() )
			return true;
		else 
			return false;
	}
}
