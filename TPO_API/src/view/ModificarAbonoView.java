package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import model.AbonoView;
import controlador.Sistema;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JSeparator;

public class ModificarAbonoView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -105786742663656611L;
	private JPanel contentPane;
	private JTextField horas;
	private JTextField precio;
	private JTextField descuento;
	private JComboBox abonos;
	private List<AbonoView> abonosView;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModificarAbonoView frame = new ModificarAbonoView();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModificarAbonoView() {
		setTitle("Modificacion de Abono");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDni = new JLabel("Nombre");
		lblDni.setBounds(35, 39, 66, 21);
		contentPane.add(lblDni);
		
		JButton bModificar = new JButton("MODIFICAR");
		bModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (!abonos.getSelectedItem().equals("Seleccione")){
					
					if ( !vacio() && Sistema.getInstancia().esNumerico(horas.getText()) &&
							Sistema.getInstancia().esNumerico(precio.getText()) && Sistema.getInstancia().esNumerico(descuento.getText()) ){
						
						int h;
						float p;
						float d;
						boolean fueraRango = false;
						
						try {
							h = Integer.parseInt(horas.getText());
							p = Float.parseFloat(precio.getText());
							d = Float.parseFloat(descuento.getText());
							Sistema.getInstancia().modificarAbono(abonos.getSelectedItem().toString(),h,p,d);
						} catch (NumberFormatException e1) {
							fueraRango = true;
						}
					
						if ( !fueraRango )
							close();
						else
							JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inválidos");
							
					}
					else
						JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inválidos");
					
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un abono");
					
				
				
			}
		});
		bModificar.setBounds(200, 278, 105, 23);
		contentPane.add(bModificar);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				close();
			}
		});
		bCancelar.setBounds(362, 278, 105, 23);
		contentPane.add(bCancelar);
		
		JLabel label = new JLabel("Horas");
		label.setBounds(35, 112, 66, 21);
		contentPane.add(label);
		
		horas = new JTextField();
		horas.setColumns(10);
		horas.setBounds(104, 112, 94, 20);
		contentPane.add(horas);
		
		JLabel label_1 = new JLabel("Precio");
		label_1.setBounds(35, 161, 66, 21);
		contentPane.add(label_1);
		
		precio = new JTextField();
		precio.setColumns(10);
		precio.setBounds(104, 162, 94, 20);
		contentPane.add(precio);
		
		JLabel label_2 = new JLabel("Descuento");
		label_2.setBounds(35, 211, 66, 21);
		contentPane.add(label_2);
		
		descuento = new JTextField();
		descuento.setColumns(10);
		descuento.setBounds(104, 211, 94, 20);
		contentPane.add(descuento);
		
		abonos = new JComboBox();		
		abonos.setBounds(104, 39, 152, 20);
		contentPane.add(abonos);
		abonos.addItem("Seleccione");
		
		JSeparator separator = new JSeparator();
		separator.setBounds(35, 85, 581, 2);
		contentPane.add(separator);
		
		abonosView = Sistema.getInstancia().buscarAbonosView();
		for (AbonoView a : abonosView)
			abonos.addItem( a.getNombre() );
		
		abonos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !abonos.getSelectedItem().equals("Seleccione") ){
					
					for (AbonoView a : abonosView)
						if ( a.getNombre().equals(abonos.getSelectedItem().toString()) ){
							
							horas.setText( String.valueOf(a.getHoras()) );
							precio.setText( String.valueOf(a.getPrecio()) );
							descuento.setText( String.valueOf(a.getDescuento()) );
							break;

						}					
					
				}
			}
		});
		
	}
	
	public void close(){
		this.dispose();		
	}
	
	public boolean vacio(){
		if (horas.getText().isEmpty() ||  precio.getText().isEmpty() || descuento.getText().isEmpty())
			return true;
		else
			return false;
	}
}
