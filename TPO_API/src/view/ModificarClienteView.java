package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import model.ClienteView;
import controlador.Sistema;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JSeparator;

public class ModificarClienteView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4203049338995558566L;
	private JPanel contentPane;
	private JTextField nombre;
	private JTextField domicilio;
	private JTextField telefono;
	private JTextField mail;
	private boolean encontrado;
	private JComboBox clientes;
	private List<ClienteView> clientesView;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModificarClienteView frame = new ModificarClienteView();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModificarClienteView() {
		setTitle("Modificacion de Cliente");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("DNI");
		label.setBounds(35, 30, 66, 21);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Nombre");
		label_1.setBounds(35, 98, 66, 21);
		contentPane.add(label_1);
		
		nombre = new JTextField();
		nombre.setColumns(10);
		nombre.setBounds(101, 99, 194, 20);
		contentPane.add(nombre);
		
		JLabel label_2 = new JLabel("Domicilio");
		label_2.setBounds(35, 142, 66, 21);
		contentPane.add(label_2);
		
		domicilio = new JTextField();
		domicilio.setColumns(10);
		domicilio.setBounds(101, 143, 194, 20);
		contentPane.add(domicilio);
		
		JLabel label_3 = new JLabel("Telefono");
		label_3.setBounds(35, 190, 66, 21);
		contentPane.add(label_3);
		
		telefono = new JTextField();
		telefono.setColumns(10);
		telefono.setBounds(101, 191, 194, 20);
		contentPane.add(telefono);
		
		JLabel label_4 = new JLabel("Mail");
		label_4.setBounds(35, 237, 66, 21);
		contentPane.add(label_4);
		
		mail = new JTextField();
		mail.setColumns(10);
		mail.setBounds(101, 238, 194, 20);
		contentPane.add(mail);
		
		JButton bModificar = new JButton("MODIFICAR");
		bModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !clientes.getSelectedItem().equals("Seleccione") ){
					
					if (!vacio() && Sistema.getInstancia().esNumerico(telefono.getText()) ){
						
						int dni;
						int tel;
						boolean fueraRango = false;
						
						try {
							dni = Integer.valueOf( clientes.getSelectedItem().toString() );
							tel = Integer.parseInt(telefono.getText());						
							Sistema.getInstancia().modificarCliente(dni,nombre.getText(),domicilio.getText(),tel,mail.getText());
						} catch (NumberFormatException e1) {
							fueraRango = true;
						}
						
						if ( !fueraRango) 
							close();
						else
							JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inválidos");
						
					}
					else
						JOptionPane.showMessageDialog(null, "Alguno de los datos ingresados son inválidos");
					
				}
				else
					JOptionPane.showMessageDialog(null, "Seleccione un DNI");

				
			}
		});
		bModificar.setBounds(201, 296, 105, 23);
		contentPane.add(bModificar);
		
		JButton bCancelar = new JButton("CANCELAR");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		bCancelar.setBounds(363, 296, 105, 23);
		contentPane.add(bCancelar);
		
		clientes = new JComboBox();
		
		clientes.setBounds(101, 30, 194, 20);
		contentPane.add(clientes);
		clientes.addItem("Seleccione");
		
		JSeparator separator = new JSeparator();
		separator.setBounds(35, 76, 581, 2);
		contentPane.add(separator);
		
		clientesView = Sistema.getInstancia().buscarClientesView();
		
		for (ClienteView c : clientesView)
			clientes.addItem( String.valueOf(c.getDNI()) );
		
		clientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if ( !clientes.getSelectedItem().equals("Seleccione") ){
					
					for (ClienteView c : clientesView)
						if ( c.getDNI() == Integer.valueOf(clientes.getSelectedItem().toString()) ){
							
							nombre.setText( c.getNombre() );
							domicilio.setText( c.getDomicilio() );
							telefono.setText( String.valueOf( c.getTel()) );
							mail.setText( c.getMail() );
							break;
						}
						
					
					
				}
			}
		});
		
		
	}
	
	public void limpiarPantalla(){
		
		nombre.setText("");
		domicilio.setText("");
		telefono.setText("");
		mail.setText("");
	}
	
	public boolean vacio(){
		if (nombre.getText().isEmpty() || domicilio.getText().isEmpty() || 
				telefono.getText().isEmpty() || mail.getText().isEmpty())
			return true;
		else
			return false;
	}
	
	public void close(){
		this.dispose();
	}
}
