package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controlador.Sistema;

public class ModificarCocheraView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4075399101247585340L;
	private JPanel contentPane;
	private JTextField nroCochera;
	private JComboBox cboHabil;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModificarCocheraView frame = new ModificarCocheraView();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModificarCocheraView() {
		setResizable(false);
		setTitle("Modificar cochera");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 350, 180);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNmeroDeCochera = new JLabel("Cochera");
		lblNmeroDeCochera.setBounds(22, 13, 63, 26);
		contentPane.add(lblNmeroDeCochera);
		
		nroCochera = new JTextField();
		nroCochera.setBounds(87, 15, 107, 22);
		contentPane.add(nroCochera);
		nroCochera.setColumns(10);
		
		JLabel lblEstado = new JLabel("Habilitar");
		lblEstado.setBounds(22, 50, 78, 26);
		contentPane.add(lblEstado);
		
		contentPane.add(cboHabil= new JComboBox());
		cboHabil.setModel(new DefaultComboBoxModel(new String[] {"Verdadero", "Falso"}));
		cboHabil.setBounds(87, 52, 107, 22);
		
		
		JButton btnModificar = new JButton("MODIFICAR");
		btnModificar.setBounds(52, 106, 107, 25);
		
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (!vacio() && Sistema.getInstancia().esNumerico(nroCochera.getText().trim())){
					String estado = (String) cboHabil.getSelectedItem();
					int nCochera = Integer.parseInt(nroCochera.getText());	
					Sistema.getInstancia().modificarCochera(nCochera, estado);
					JOptionPane.showMessageDialog(null, "Cochera Actualizada");
					close();
				}else
					JOptionPane.showMessageDialog(null, "Cochera inv�lida");
			}
		});
		contentPane.add(btnModificar);
		
		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		btnCancelar.setBounds(185, 106, 107, 25);
		contentPane.add(btnCancelar);
		
	}
	
	public boolean vacio(){
		if (nroCochera.getText().isEmpty())
			return true;
		else
			return false;
	}
	
	public void close(){
		this.dispose();
	}
}

